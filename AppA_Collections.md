# Collection

`Collection` è una classe astratta; qui sotto 3 metodi factory fra i più usati
```
Collection.newFrom(aCollection)
Collection.with( ... args)
Collection.fill(size, function)
```
alla documentazione per gli *istance method*. Possiamo inoltreiterare con
```
List[1, 2, 3, 4].do({ arg item, i; item.postln });
```

## array

Un array possiede un *fixed maximum size* le `List` crescono a piacere; qui sotto un array di simboli non modificabile *run-time*
```
a = #[foo, bar]
```
se invece scriviamo 
```
a = [foo, bar]
```
è modificabile ed i due nomi devono corrispondere a variabili definite in precedenza. Possiamo costruire un array nei seguenti interessanti modi
```
Array.with(1, 2, 3);
Array.fill(4, { arg i; i * 2 });
Array.series(10,1,2); // [1, 3, ... , 19]
```
possiamo venire e passare ad altre *collection*
```
[1, 2, 3, 4, 3, 2].as(Set);
```
Per girare un array
```
[1, 3, 4].reverse; // [ 4, 3, 1 ]
```

Una array da `8` elementi casuali in un intervallo prefissato
```
Array.rand(8, 1, 100);
```
Si comnfrontino i seguenti *plot*
```
Array.rand(100, 1, 100).plot;
Array.linrand(100, 1, 100).plot;
Array.exprand(100, 1, 100).plot;
```
Per la concatenazione
```
(
var y, z;
z = [1, 2, 3, 4];
y = z ++ [7, 8, 9];
z.postln;
y.postln;
)
``` 
Mentre
```
(
v = [1, 2, 3] + [1, 2];
v == [2, 4, 4];
)
```
ritorna `true`; per gli *instance method* rimandiamo alla documentazione.

## set

Accenniamo qui ai `Set` in quanto hanno i `Dictionary` come estensione; un `Set` non possiede duplicati
```
a = Set[1, 2, 2, 3]
-> Set[ 3, 2, 1 ]
```

## dictionary

Un dizionario è un array associativo, due chiavi coincidono - *match* - se il confronto con `==` ritorna `true` infatti è un `Set`; ecco come si può procedere per crearne uno
```
d = Dictionary.new;
d = Dictionary[\a -> 1, \b -> 2, \c -> 4];
d = Dictionary.newFrom(List[\a, 1, \b, 2, \c, 4]);
```
nel secondo caso vediamo una sintassi analoga all'array; possiamo scrivere
```
id = Dictionary.new;
id.put(\foo, "bar");
```
cioè `id.add(\foo -> "bar")`. Una `Association` è un oggetto del tipo `\a -> 1` con cui associamo una *key* ad un *value*.

# identity dictionary

Per capire subito basta provare
```
d = IdentityDictionary["ciao" -> 1];
d["ciao"]
```
infatti `"ciao" == "ciao"` ma è falso `"ciao" === "ciao"`, per questo motivo le *key* sono simboli
```
d = IdentityDictionary[\ciao -> 1];
d[\ciao]
```
per questo motivo un tale oggetto è molto più vicino ad un `Array` di quanto lo sia un semplice `Dictionary`. Questa classe diviene interessante per i campi `proto` e `parent`, quest'ultimo permette di creare gerarchie
```
d = IdentityDictionary.new(parent: IdentityDictionary[(\p -> 2)]);
d.put(\d, 12);
d[\d];
d[\p];
d;
d.parent;
```
Per il dettaglio, al solito, la documentazione.

## environment

Ovvero un  `IdentityDictionary` che lavora come un *namespace*; si esegua linea per linea il codice seguente
```
p = Environment.new;
p[\a] = 10;
Environment.push(p);
~a === p[\a];
Environment.pop;
~a;
```



