(
SynthDef(\woodblock, { |freq=1000, out=0|
	var sig, env, noise, envnoise;
	env = EnvGen.ar(Env.perc(0.01, 0.05), doneAction: 2);
	envnoise = EnvGen.ar(Env.perc(0.01, 0.005), doneAction: 2);
	sig = Klang.ar(
		`[
			[1828.12, 1875.00, 1781.25, 46.87, 1921.87, 5250.00],
			[0.47, 0.43, 0.36, 0.31, 0.25, 0.2]
		]);
	noise = PinkNoise.ar()*envnoise*0.5;
	// sig = sig + noise;
	sig = sig*env;
	sig = LPF.ar(sig, freq);
	Out.ar(out, sig!2);
}).add
)

{PinkNoise.ar()}.play;
Env.perc(0.01, 0.5, 1, -8).test.plot;

Synth(\woodblock, [\freq, 2000]);

(
SynthDef.new(\reverb, { |outBus = 0, inBus|
    var input;
    input = In.ar(inBus, 1);
    16.do({ input = AllpassL.ar(input, 0.04, { Rand(0.001,0.04) }!2, 3)});
    Out.ar(outBus, input);
}).add;
)


b = Bus.audio(s,1);
x = Synth.new(\reverb, [\inBus, b]);
y = Synth.before(x, \woodblock, [\out, b]);

s.freqscope;



