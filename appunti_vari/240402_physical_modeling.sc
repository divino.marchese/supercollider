/****** Decay1, Decay2 ************************************/

/*

tratto da: https://bzoennchen.github.io/supercollider-book/chapters/sounddesign/physical-modeling.html

una sorta di shamisen
*/


{ Decay.ar(Impulse.ar(1), 0.01) }.plot(0.5);
{ Decay2.ar(Impulse.ar(1), 0.01) }.plot(0.5);

s.plotTree

(
{
    var exciter, decay, noise;
    exciter = Impulse.ar(0!2);
    decay = Decay2.ar(exciter, 0.008, 0.2);
    noise = LFNoise2.ar(3000) * decay;
    DetectSilence.ar(noise, doneAction: Done.freeSelf);
    noise
}.play;
)

/*************** Delay1, Delay2, DelayN ********************/

{ LFPulse.ar(freq: 1, width: 0.5) }.plot(4);
{ LFPulse.ar(freq: 1, width: 0.2) }.plot(4);
{ LFPulse.ar(freq: 1, width: 0.1) }.plot(4);


(
{
var sig;
sig = LFPulse.ar(freq: 1, width: 0.1);
sig = sig - Delay1.ar(sig)
}.plot(4);
)

(
{
var sig;
sig = LFPulse.ar(freq: 1, width: 0.1);
sig = sig - Delay2.ar(sig)
}.plot(4);
)

// DelayN in cui il ritardo diventa significativo

(
{
var sig;
sig = LFPulse.ar(freq: 1, width: 0.1);
sig = sig - DelayN.ar(sig, 0.1, 0.1, 0.5)
}.plot(4);
)


/****** u esempio di sintesi ****************/

// step 1: senza feedback positivo

(
SynthDef(\step1, {
    var sig, exciter;

    exciter = Impulse.ar(0!2);
    sig = WhiteNoise.ar() * exciter;
    sig = DelayN.ar(sig, 0.01,
        delaytime: \delaytime.kr(0.002)) * \beta.kr(0.95);
    DetectSilence.ar(sig, doneAction: Done.freeSelf);
    Out.ar(0, sig);
}).add;
)

Synth(\step1, [beta: 0.98, delaytime: 0.002]);


// step 2: dintroduciamo il feedback

(
SynthDef(\step2, {
    var sig, exciter, local;

    local = LocalIn.ar(2);
    exciter = Impulse.ar(0!2);
    sig = WhiteNoise.ar() * exciter;
    sig = sig + local;

    local = DelayN.ar(sig, 0.01,
        delaytime: \delaytime.kr(0.002)) * \beta.kr(0.95);
    LocalOut.ar(local);

    DetectSilence.ar(sig, doneAction: Done.freeSelf);
    Out.ar(0, sig);
}).add;
)

Synth(\step2, [beta: 0.98, delaytime: 0.002]);
Synth(\step2, [beta: 0.98, delaytime: 1/440]);
Synth(\step2, [beta: 0.98, delaytime: 1/880]);
Synth(\step2, [beta: 1, delaytime: 1/440]);    // suono fermo

// step 3: introduzione del filtro

(
SynthDef(\step3, {
    var sig, exciter, local;

    local = LocalIn.ar(2);
    exciter = Impulse.ar(0!2);
    sig = WhiteNoise.ar() * Decay2.ar(exciter, 0.008, 0.04) * \amp.kr(1.0);
    sig = sig + local;
    sig = LPF.ar(sig, \cutoff.kr(440));

    local = DelayN.ar(sig, 0.01,
        delaytime: 1/\freq.kr(440)) * \beta.kr(0.95);

    LocalOut.ar(local);

    DetectSilence.ar(sig, doneAction: Done.freeSelf);
    Out.ar(0, sig);
}).add;
)

(
Pbind(
    \instrument, \step3,
    \dur, Pseq([0.25, 0.25, 0.5, 0.5, 0.25], inf),
    \cutoff, Pexprand(1000, 6000, length: 30),
    \beta, 0.97,
    \midinote, Prand([65, 70, 80, 54], inf),
    \amp, 1,
    \pan, (Pkey(\midinote).linlin(54, 80, -0.9, 0.9))
).play;
)



