// *** events (vedi prima serie di appunti)

Event

e = (gatti: 2, mele: 3);
e[\mele] = e[\mele] + 2;

// from documentation ~x e ~y ricordano che Evebt < Enviroment
a = (x: 6, y: 7, play: { (~x * ~y).postln });
a.play; // returns 6 * 7

().play;               // Synth "insdtrument" default  CTRL+i Event
(freq: 440).play;
(freq: 440, amp: 0.5, pan: 0.7, sustain: 0.3).play;

Synth(\default, [freq: 440]);
// usando lo strumento definito in precedenzas
(instrument: \pulse, freq: 440, amp: 0.8).play;


Event.eventTypes.keys; /// keys of eventTypes dictionary


// Pbind: per generare uno stream (routine) di eventi

(
a = Pbind(\x, Pseq([1, 2, 3]), \y, Prand([100, 300, 200], inf), \zzz, 99);
x = a.asStream;
)

// a next passiamo l'evento di default
x.next(());
x.next; // non funziona

// play events

(
a = Pbind(\freq, Pseq([440, 674, 550, 770, 2000, 3000]));
x = a.asStream;
)

x.next(()).play;

a.play;
