/**************** TRATTO DA *********************************************

https://uofi.app.box.com/s/b2hapggp6taaeac2uppqpommau3rhypv
https://www.youtube.com/playlist?list=PLPYzvS8A_rTYEba_4SDvRJyIyjKaDNjn9

*************************************************************************/


// explore class

Array.browse;


// tipi di dato

1.class
0.3.class
\a.class
"ciao".class
$a.class
true.class
false.class
(\x -> 12).class
[1, 2].class
#[1, 2].class
a = #[1, 2] // constant array
a[0] = 2
Array[1, 2].class
(1..10).class
(1..10)¨
Set[1, 2, 3].class
Set[1, 1, 2]
Dictionary[\x -> 12].class
[\x -> 12, \y -> 12].class
Dictionary.newFrom([\x, 12]).class
Dictionary.with(*[\a->1,\b->2,\c->3]) // * spread operator
Dictionary.with(\a->1,\b->2,\c->3)
IdentityDictionary["a" -> 12].class
Environment
(\a: 12).class

// test sui tipi

2.isFloat
"ciao".size

// operatori


2.pow(4)
3/2
3//2
3%2
3**2
3.pow(2)
"a" == "a"
"a" === "a" // due istanze con lo stesso contenuto non identiche
\a === \a
Dictionary["a" -> 12] == Dictionary["a" -> 12]
IdentityDictionary["a" -> 12] == IdentityDictionary["a" -> 12]
IdentityDictionary[\a -> 12] == IdentityDictionary[\a -> 12]
IdentityDictionary[\a -> 12] === IdentityDictionary[\a -> 12]




"posr".postln

a = 10 // le lettere maiuscole sono tutte variabili global alcune riservate tipo s

prova = 11 // non va
~perova = 11

// aray

x = [1, 2, 3.4]
x[0]
x.at(0)
x = (1..100)
x[99]
[1, 2, 3, 4].scramble
[1, 2, 3, 4].rotate
Array.series(5, 10, 2)
4.dup()
4.dup
4 ! 2
4 ! 4

% funzioni

f = { 33 }
f.class
f
f.value()
f.value
f.()

f = { arg n; n**2}
f = { | n |; n**2} // scrittura un po' esoterica
f.(2)
f.value(2)   // meglio in chiave funzionale

% randomness

rrand(1,10)  // range random distribuzione uniforme interi
rrand(1, 10.0) // float
exprand(1,10) // float da usare per le frequenze ad esempio all'interno di un'ottava selezionauniformemente le note, analogamente per l'ampiezza (vedi dB)
x = [1, 2, 5, 66]
x.choose()
x.choose

rrand(1,10) ! 4
{ rrand(1,10) } ! 4   // correct way
rrand(1,10).dup(4)
{ rrand(1,10) }.dup(4)
1.rrand(10)

4 + { [1, 2, 3].choose }       // no
{4 + { [1, 2, 3].choose }}     // no
{4 + [1, 2, 3].choose }
{4 + [1, 2, 3].choose }.value  // no
{4 + [1, 2, 3].choose ! 4 }.value
{{[1, 2, 3].choose} ! 4 + 12 }.value // ok

0.5.coin

// randomness plot

// uniforme
Array.fill(500, {  1.0.rand }).plot("Sequence of 500x 1.0.rand", discrete: true);
// lineare
Array.fill(500, {  1.0.linrand }).plot("Sequence of 500x 1.0.linrand", discrete: true);
// tipo Gauss a 0.5
Array.fill(500, {  1.0.sum3rand }).plot("Sequence of 500x 1.0.sum3rand", discrete: true);
// exprand (inm reltà logaritmico)
Array.fill(500, {  exprand(0.001, 1) }).plot("Sequence of 500x exprand(0.001, 1)", discrete: true);

// condizioni

(
x = [1, 0].choose;
if( x == 1, {"vero".postln }, {"falso".postln })
)

// iterazioni

(
x = (11..21);
y = x.do({arg n, i; ("number: " ++ n ++ ", index: " ++i).postln})
)

(
x = (1..10);
y = x.do({arg n; (n**2).postln})
)

(
x = (1..10);
y = x.collect({arg n; (n**2).postln})






