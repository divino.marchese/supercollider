// ******* GUI ***********************************************************

(
w = Window("title", Rect(200, 100, 300, 400));
w.alwaysOnTop_(true);
w.front;
)

Window("title", Rect(200, 100, 300, 400)).front;

Window.closeAll;

(
var win, slider, knob;
win = Window().front;
slider = Slider(win, Rect(20, 20, 40, 200));
knob = Knob(win, Rect(100, 20, 60, 60));
)

(
var win, sliders;
win = Window().front;
sliders = 8.collect({ Slider() });   // trick spread operator
win.layout_(HLayout(*sliders));
)

(
var win, sliders, buttons;
win = Window().front;
sliders = 8.collect({ Slider() });
buttons = 8.collect({ Button() });
win.layout_(GridLayout.rows(sliders, buttons));
)


(
w = Window(name: "sl", bounds: Rect(100, 100, 70, 400)).front;
// w.alwaysOnTop_(true);
~sl = Slider();
~sl.action_({arg view; view.value.linexp(0, 1, 200, 3000).postln;});
w.layout_(HLayout(~sl));
)

Slider.browse;

~sl.visible_(false);
~sl.visible_(true);

~sl.background_(Color(0.2, 0.8, 0.5));
~sl.background_(Color.rand);

~sl.value_(0.5);
~sl.value = 0.5;

// GUI e segnali di controllo

(
x = {
    var sig;
    sig = SinOsc.ar(freq: \freq.kr(200), mul: 0.2);
}.play;
)

(0..10).collect { |num| num.linexp(0, 10, 4.3, 100) };
(0..10).linexp(0, 10, 4.3, 100).plot; // equivalent.


// freccia su e giù ... con CTRL in modo discreto

(
w = Window(name: "sl", bounds: Rect(100, 100, 70, 400)).front;
// w.alwaysOnTop_(true);
~sl = Slider();
~sl.action_({
    arg view;
    x.set(\freq, view.value.linexp(0, 1, 200, 3000).postln);
});
w.onClose_({ x.free; });
w.layout_(HLayout(~sl));
)


// versione aggiornata coi gruppi

g = Group();

(
x = {
    var sig;
    sig = SinOsc.ar(freq: \freq.kr(200), mul: 0.2);
}.play(target: g);
)

(
w = Window(name: "sl", bounds: Rect(100, 100, 70, 400)).front;
// w.alwaysOnTop_(true);
~sl = Slider();
~sl.action_({
    arg view;
    x.set(\freq, view.value.linexp(0, 1, 200, 3000).postln);
});
w.onClose_({ g.freeAll(); });
w.layout_(HLayout(~sl));
)


// introduciamo il ritardo

s.meter;

(
x = {
    var sig;
    sig = SinOsc.ar(freq: \freq.kr(200).lag(0.1), mul: 0.2); // see also defer
}.play;
)

(
x = {
    var sig;
    sig = Lag.ar(in: SinOsc.ar(freq: \freq.kr(200), mul: 0.2), lagTime: 0.1);
}.play;
)

(
w = Window(name: "sl", bounds: Rect(100, 100, 70, 400)).front;
~sl = Slider();
~sl.action_({
    arg view;
    x.set(\freq, view.value.linexp(0, 1, 200, 3000).postln);
});
w.onClose_({ x.free; });
w.layout_(HLayout(~sl));
)









