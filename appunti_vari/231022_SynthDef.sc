// convert a UGen function in a SynthDef

(
~fn = {
    arg gate = 1, amp = 0.2;
    var sig, env;
    sig = SinOsc.ar(ExpRand(200, 1000), mul: amp) ! 2;
    env = EnvGen.kr(
        envelope: Env(
            levels: [0, 1, 0.3, 0],
            times: [0.5, 0.6, 2],
            curves: [-3, -1, -1],
            releaseNode: 2
        ),
        gate: gate,
        doneAction: 2
    );
    sig = sig * env;
};
)

x = ~fn.play;
x.set(\gate, 0);
x.free();

(
// SynthDef.new() ...
SynthDef(\tone, {
    arg gate = 1, amp = 0.2;
    var sig, env;
    sig = SinOsc.ar(ExpRand(200, 1000), mul: amp) ! 2;
    env = EnvGen.kr(
        envelope: Env(
            levels: [0, 1, 0.3, 0],
            times: [0.5, 0.6, 2],
            curve: [-3, -1, -1],
            releaseNode: 2
        ),
        gate: gate,
        doneAction: 2
    );
    sig = sig * env;
    Out.ar(0, sig); // 0: lest 1:right
}).add;
)

[gate: 1].class
[\gate, 1] == [gate: 1]
// x = Synth(\tone, [\gate, 1]);
x = Synth(\tone, [gate: 1]);
x.set(\gate, 0);