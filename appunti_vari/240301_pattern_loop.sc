// evento con Synth di default (dente di sega)


/*

STream possiede i metodi next e reset
Routine sono STream e i pattern possono diventare Stream

*/

// ebent type note

(freq: 440).play;

(
a = 7.asStream;
a.postln;
a.next.postln;
)


(
r = Routine {
    var    i = 0;
    loop {
        i.yield;
        i = i + 1;
    };
};
)

r.nextN(5);
r.reset;

a = r { 1.yield; 2.yield };
a.next;
a.reset;

// Pseries, serie aritmetica, asStream istanzia una routine

~r1 = Pseries(0, start: 1).asStream;
~r1.nextN(5);

~r2 = Pseries(0, 1, length: 3).asStream;
~r2.nextN(5);




// Pseq

~r2 = Pseq(#[1, 2, 3], repeats: 4, offset: 1).asStream;
~r2.nextN(7);
~r2.reset;
~r2.all;

// Pslide

Pslide(#[1, 2, 3, 4, 5, 6, 7, 8], repeats: 6, len: 3, step: 1, start: 0, wrapAtEnd: false).asStream.all;

// un range array

(-6, -4 .. 12)

(
p = Pbind(
    \degree, Pslide((-6, -4 .. 12), 8, 3, 1, 0),
    \dur, Pseq(#[0.1, 0.1, 0.2], inf),
    \sustain, 0.15   // fade dopo la durata
).play;
)

// Pbind: combines several value streams into one event stream

(
p = Pbind(
    \degree, Prand([0, 0, 1, 2, 4, 5], inf),
    \dur, 0.25
).play;
)

p.stop;

(
Pbind(
    \degree, Pseq(#[0, 0, 4, 4, 5, 5, 4], 1),
    \dur, Pseq(#[0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 1], 1)
).play;
)

// Pshuf ... mescola e mantiene il mescolamento

(
p = Pbind(
    \degree, Pshuf([0, 1, 2, 4, 5], inf),
    \dur, 0.25
).play;
)

p.stop;

// Pwrand: usa i pesi

(
p = Pbind(
    \degree, Pwrand((0..7), [4, 1, 3, 1, 3, 2, 1].normalizeSum, inf),
    \dur, Prand([0.25, 1, 0.5], inf)
).play;
)

p.stop;

// Place:

Place(#[1, [2, 3], [4, 5, 6]], 3).asStream.all;
Place(#[[0, 1], [2,3], [4, 5]], 2).asStream.all;


(0 .. 8)

(
p = Pbind(
    \degree, Place(#[0, [1, 4], [3, 5, 8]], 3),
    \dur, 0.25
).play;
)

// Ppatlace, come sopra solo che con i pattern, un ottimo modo per alternare i pattern

(
Ppatlace([
        Pseq([0, 0, 0, 4], 1),
        Pseries(1, 1, 4)
    ], inf).asStream.all;
)

// Hanon exercise
(
p = Pbind(
    \degree, Ppatlace([
        Pseries(0, 1, 8),    // first, third etc. notes
        Pseries(2, 1, 7)    // second, fourth etc. notes
    ], inf),
    \dur, 0.25
).play;
)

// Pgeom: serie geometrica (vedi Pseq serie aritmetica)

Pgeom(1, 2, inf).asStream.nextN(100);
Pgeom(0.5, 2, inf).asStream.nextN(100);
Pgeom(1.0, 1.1, inf).asStream.nextN(100);

(
var a;
a = Pgeom(1.0, 1.1, inf);
a.asStream.nextN(100).plot;
)

(
p = Pbind(
    \degree, Pseries(-7, 1, 15),
    \dur, Pgeom(0.5, 0.89140193218427, 15)
).play;
)


// Prout ... da una routine ad un pattern

(
var a;
a = Prout({ loop { 1.yield; 2.yield; 7.yield; 10.do { 1.0.rand.yield } }});
a.asStream.nextN(100);
)

// Pfunc

(
var a, x;
a = Pfunc({ exprand(0.1, 2.0) + #[1, 2, 3, 6].choose }, { \reset.postln });
x = a.asStream;
x.nextN(20).postln;
x.reset;
)


(
    SynthDef(\help_SPE2, { arg i_out=0, sustain=1, freq;
        var out;
        out = RLPF.ar(
            LFSaw.ar( freq ),
            LFNoise1.kr(1, 36, 110).midicps,
            0.1
        ) * EnvGen.kr( Env.perc, levelScale: 0.3,
            timeScale: sustain, doneAction: Done.freeSelf );
        //out = [out, DelayN.ar(out, 0.04, 0.04) ];
        4.do({ out = AllpassN.ar(out, 0.05, [0.05.rand, 0.05.rand], 4) });
        Out.ar( i_out, out );
    }).add;
)
(
// streams as a sequence of pitches
    var pattern, streams, dur, durDiff;
    dur = 1/7;
    durDiff = 3;
    pattern = Prout.new({
        loop({
            if (0.5.coin, {
                #[ 24,31,36,43,48,55 ].do({ arg fifth; fifth.yield });
            });
            rrand(2,5).do({
                // varying arpeggio
                60.yield;
                #[63,65].choose.yield;
                67.yield;
                #[70,72,74].choose.yield;
            });
            // random high melody
            rrand(3,9).do({ #[74,75,77,79,81].choose.yield });
        });
    });
    streams = [
        (pattern - Pfunc.new({ #[12, 7, 7, 0].choose })).midicps.asStream,
        pattern.midicps.asStream
    ];
    Routine({
        loop({
            Synth( \help_SPE2, [ \freq, streams.at(0).next, \sustain, dur * durDiff ] );
            durDiff.do({
                Synth( \help_SPE2, [ \freq, streams.at(1).next, \sustain, dur ] );
                dur.wait;
            });
        })
    }).play
)








