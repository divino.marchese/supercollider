// buffers

Platform.resourceDir
Platform.userHomeDir

// a stereo buffer: 8 second stereo buffer
s.boot;
b = Buffer.alloc(s, s.sampleRate * 8.0, 2);
b.free;

// per lavorare su . (directory corrente)

"mio".resolveRelative

// caricamento del buffer (suoni messi a disposizione nell'installazione)

p = Platform.resourceDir +/+ "sounds/a11wlk01.wav";
b = Buffer.read(s, p);
b.play; // doneAction: 2 implicit

b
b.duration
b.bufnum
b.numChannels
b.query
b.free
Buffer.freeAll // liberare la memoria dai buffer caricati

s.plotTree;

(
SynthDef.new(\play, {
    arg buf, out = 0, rate = 1;
    var sig;
    sig = PlayBuf.ar(
        numChannels: buf.numChannels,  // 1
        bufnum: buf,            // riceve buf.bufnum
        // rate: 44100/188893
        rate: BufRateScale.kr(buf) * rate,
        // loop: 1 ... il buffer viene ricaricato
        doneAction: 2
    );
    sig = sig ! 2;
    Out.ar(out, sig);
}).add;
)

// intervallo in semitoni in rapporto
Synth.new(\play, [\buf, b, \rate, 4.midiratio]);

// con inviluppo su cui metetre doneAction 2

(
SynthDef.new(\play, {
    arg buf, out = 0, rate = 1;
    var sig, env;
    sig = PlayBuf.ar(
        numChannels: buf.numChannels, // 1
        bufnum: buf,
        // rate: 44100/188893
        rate: BufRateScale.kr(buf) * rate,
        // startPos: 0
        loop: 1
    );
    env = EnvGen.kr(
        Env(
            [0, 1, 0],
            [0.02, 1],
            [-2, -4]
        ),
        doneAction: 2
    );
    sig = sig ! 2;
    sig = sig * env;
    Out.ar(out, sig);
}).add;
)

Synth.new(\play, [\buf, b]);
Synth.new(\play, [\buf, b, \rate, 4.midiratio]);
Synth.new(\play, [\buf, b, \rate, 12.midiratio]);

// usare i symbol all'interno di un SynthDef come argomenti, per passare due argomenti
// usare \kr(1, 2) ad esempio

(
SynthDef.new(\play, {
    arg buf, out = 0;
    var sig, env;
    sig = PlayBuf.ar(
        numChannels: buf.numChannels, // 1
        bufnum: buf,
        // rate: 44100/188893
        rate: BufRateScale.kr(buf) * \rate.kr(1),
        // startPos: 0
        loop: 1
    );
    env = EnvGen.kr(
        Env(
            [0, 1, 0],
            [\atk.ir(0.02), \rel.ir(1)],  \\ ir sono segnali per argomenti (initial rate)
            [-2, -4]
        ),
        doneAction: 2
    );
    sig = sig ! 2;
    sig = sig * env;
    Out.ar(out, sig);
}).add;
)

Synth.new(\play, [\buf, b]);

// start position

(
SynthDef.new(\play, {
    arg buf, out = 0, rate = 1, pos = 0;
    var sig, env;
    sig = PlayBuf.ar(
        numChannels: buf.numChannels, // 1
        bufnum: buf,
        // rate: 44100/188893 = s.sampleRate
        rate: BufRateScale.kr(buf) * rate,
        startPos: s.sampleRate * pos,
        loop: 1
    );
    env = EnvGen.kr(
        Env(
            [0, 1, 0],
            [0.02, 1],
            [-2, -4]
        ),
        doneAction: 2
    );
    sig = sig ! 2;
    sig = sig * env;
    Out.ar(out, sig);
}).add;
)

Synth.new(\play, [\buf, b, \pos, 0]);

// simultanei

(
[1, 3, 6, 12].do{
    arg n;
    n = n + [11, 12, 15].choose;
    Synth.new(\play, [\buf, b, \rate, n.midiratio]);
}
)


// routine

(
r = Routine.new({
    [-7, -5, 2].do({
        arg n;
        Synth.new(\play, [\buf, b, \rate, n.midiratio]);
        wait(rrand(0.1, 0.7));
        yield(rrand(0.1, 0.7));
    })
})
)

r.play;  // tutto in una volta
r.next;  // un passo alla volta
r.reset;

// altri esempi e routine da altre routine

// p = "sounds/metal_hit.aiff".resolveRelative;
p = "sounds/glass.wav".resolveRelative;
b = Buffer.read(s, p);
b.play;
s.sampleRate
b.numFrames
~durata = s.sampleRate / b.numFrames;
Buffer.freeAll;

(
SynthDef.new(\play, {
    arg buf, out = 0, rate = 1, pos = 0;
    var sig, env;
    sig = PlayBuf.ar(
        numChannels: buf.numChannels, // 1
        bufnum: buf,
        // rate: 44100/188893 = s.sampleRate
        rate: BufRateScale.kr(buf) * rate,
        startPos: s.sampleRate * pos,
        loop: 1
    );
    env = EnvGen.kr(
        Env(
            [0, 1, 0],
            [0.02, 0.8],
            [-2, -4]
        ),
        doneAction: 2
    );
    sig = sig ! 2;
    sig = sig * env;
    Out.ar(out, sig);
}).add;
)

(
r = Routine.new({
    [-3, -1, 1, 2, 4, 6, 8, 9].do({
        arg n;
        Synth.new(\play, [\buf, b, \rate, n.midiratio]);
        // wait(rrand(0.1, 0.7));
        yield(rrand(0.3, 1.0));
    })
})
)

r.play;  // tutto in una volta
r.next;  // un passo alla volta
r.reset;

(
~noteGen = Routine({
    var pch;
    inf.do({
        pch = -8;
        8.do({
            pch.yield;
            pch = pch + rrand(1, 6);
        });
    });
})
)

~noteGen.next;

(
~soundGen = Routine({
    inf.do({
        Synth.new(\play, [\buf, b, \rate, ~noteGen.next.midiratio]);
        0.25.yield;
    });
})
)

~soundGen.next;




