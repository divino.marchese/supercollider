s.boo;

b = Buffer.read(s, "pvc.aiff".resolveRelative);
// test buffer
{ PlayBuf.ar(1, b, BufRateScale.kr(b), doneAction: 2)}.play;

s.plotTree;

(
SynthDef(\play,{
    var sig, buf;
    buf = \buf.ir(0);     // initializationsignal
    sig = PlayBuf.ar(1!2, buf, BufRateScale.ir(buf) * \rate.ir(1), doneAction: 2);
    sig = sig * \amp.kr(0.5);
    Out.ar(\out.ir(0), sig);
}).add;
)

x = Synth(\play, [buf: b, rate: 8.midiratio]);
x.free;

// il server come lavora internamente, doc "Server Command Reference"

// send an OSC-message to the server
// s.sendMsg


s.sendMsg("/s_new", "default", s.nextNodeID, 0, 1); // nextNodeID get a unique node ID
s.plotTree;

// 0 -> add the new node to the the head of the group specified by the add target ID
s.sendMsg("/s_new", "default", 12345, 0, 1);
s.sendMsg("/n_set", 12345, "freq", 800);
s.sendMsg("/n_set", 12345, "gate", 0);
s.sendMsg("/n_free", 12345);
s.sendMsg("/n_run", 12345, 0);     // start
s.sendMsg("/n_run", 12345, 1);     // stop

// alcuni dati sul server

NetAddr.langPort;   // 57120



// definiamo un custome message

(
OSCdef(\receiver, {
    arg msg;
    msg.postln
}, "/test")
)

~mys.sendMsg("/test", 'ciao');
~mys.sendMsg("/test", [1, 2, 2]);


(
OSCdef(\receiver, {
    arg msg;
    Synth(\play, [buf: b, rate: rrand(-12.0, 0).midiratio]);
}, "/test")
)

(
OSCdef(\receiver, {
    arg msg, amp;
    msg.postln;
    amp = msg[2];
    amp = amp.clip(0.0, 0.5);  // see Clip Ugen
    Synth(\play, [buf: b, amp: amp, rate: msg[1].midiratio]);
}, "/test")
)

OSCdef.all; // see all definition
OSCdef.freeAll;

~mys.sendMsg("/test", 20, 0.5);

(
OSCdef(\xy, {
    arg msg;
    msg.postln;
}, "/xy")
)

~mys = NetAddr("192.168.8.2", 57120);
~mys.sendMsg("/xy", 20, 0.5);

// per gestire più segnali in contemporanea (tratto da appunti)

// mix fra symbol e string
~s1 = \ciao ++ \coa;
~s1.class;
~s1 = ~s1.asSymbol;
~s1.class;
~s2 = \ciao ++ 'ciao'
~s2.class;


(
20.collect({ |i|
	OSCdef(
		key: ("receiver" ++ (i.asString)).asSymbol,
		func: {
			arg msg;
			msg.postln;
			if(
				msg[1] == 1,
				{
					Synth(\play, [buf: b, amp: 0.2, rate: (i - 19).midiratio])
				}
			)
		},
		path: "/1/multipush1/" ++
		(i.div(5) + 1).asString ++
		"/" ++
		(i.mod(5) + 1).asString
	);
});
)



