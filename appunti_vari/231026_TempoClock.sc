// ******* scheduling events


// default tempo clock
TempoClock.default.tempo;
t = TempoClock.default;
t.beats;


// tempo = beats/secons

(
t = TempoClock.new(tempo: 2, beats: 5);
"current beats:" + t.beats;
// t.isRunning;
// t.stop;
)

(
t = TempoClock.new();

t.sched(3, {
    t.beats = 100;
    t.beats.postln; // still 3
    // t.stop;
    nil
});

// t.stop;
)

t = TempoClock.new(72/60); // 72 beats (battioti) al minuto
(
t = TempoClock.new(72/60);  // sopravvive a
t.permanent = true;
// t = TempoClock.new(72/60).permanent_(true): SETER
)
t.permanent;
t.isRunning;
t.beats;        // SHIFT + RETURN per ripetere il tutto
t.beatDur;
t.baseBar;

t.seconds;
t.stop;   // remove thre clock

// OOP
// see documentation for getter and setter

t.sched(4, {"hi".postln})

/* con play(t) possiamo usare il tempo clock







