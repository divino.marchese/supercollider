// playng a function: a Lag example

(
~fn = {
    arg freq, delay = 2;
    var sig;
    // freq = freq.lag(delay);
    freq = Lag.kr(freq, delay);
    sig = SinOsc.ar(freq: freq, mul: 0.1);
    sig = sig ! 2;
};
)

x = ~fn.play;
x.set(\freq, exprand(200, 2000));
x.free;

// waves

(
~fn = {
    arg amp = 0.2;
    var sig, mod;
    mod = SinOsc.kr(freq: 1/4, phase: 3pi/2, mul: 0.5, add: 0.5);
    sig = PinkNoise.ar(amp ! 2);
    sig = sig * mod;
};
)

x = ~fn.play;
x.release(5);

// nodes management

s.plotTree;

(
~fn = {
    arg amp = 0.2;
    var sig, env;
    sig = SinOsc.ar(ExpRand(200, 1000), mul: amp) ! 2;
    env = Line.kr(1, 0, 0.2, doneAction: 2);
    sig = sig * env;
};
)

x = ~fn.play;
x.free();

s.plotTree;

// envelope per un suono flautato
(
envelope: Env(
    levels: [0, 1, 0.1, 0.0, 0],
    times: [0.5, 0.1, 2, 0.5],
    curve: [0, 0, 0, 0]
).plot;
)

(
~fn = {
    arg amp = 0.2;
    var sig, env;
    sig = SinOsc.ar(ExpRand(200, 1000), mul: amp) ! 2;
    env = EnvGen.kr(
        envelope: Env(
            levels: [0, 1, 0.1, 0.0, 0],
            times: [0.5, 0.1, 2, 0.5],
            curve: [0, 0, 0, 0]
        ), doneAction: 2
    );
    sig = sig * env;
};
)

x = ~fn.play;
x.free();

// gate e segnali per mantenere il sustain di un inviluppo
// 1 start sustain (open the gate) 0 relaease (close the gate)
// doneAction 0 : we can reopen the gate


(
~fn = {
    arg gate = 1, amp = 0.2;
    var sig, env;
    sig = SinOsc.ar(ExpRand(200, 1000), mul: amp) ! 2;
    env = EnvGen.kr(
        envelope: Env(
            levels: [0, 1, 0.3, 0],
            times: [0.5, 0.6, 2],
            curve: [-3, -1, -1],
            releaseNode: 2      // mantaing 0.3 [2]
        ),
        gate: gate,
        doneAction: 2
    );
    sig = sig * env;
};
)

x = ~fn.play;
x.set(\gate, 0);
x.free();

// t signal ... (a lterantive to a gate)
// vedi SynthDef help trigger rate

(
~fn = {
    arg t_gate = 1, amp = 0.2;
    var sig, env;
    sig = SinOsc.ar(ExpRand(200, 1000), mul: amp) ! 2;
    env = EnvGen.kr(
        envelope: Env(
            levels: [0, 1, 0],
            times: [0.02, 1],
            curve: [-2, -4],
        ),
        gate: t_gate,
        doneAction: 0   // note 0
    );
    sig = sig * env;
};
)

x = ~fn.play;
x.set(\t_gate, 1);    // the same notes
x.free();

s.plotTree()




