// decay: a percussive sound
{ Impulse.ar(1) }.play;
{ Decay.ar(Impulse.ar(1), 0.1, WhiteNoise.ar) }.play

// we then wrap that noise in a fast repeating delay
(
{
	CombL.ar(
		Decay.ar(Impulse.ar(1), 0.1, WhiteNoise.ar),
		0.02,   // max delay time
		0.0022727272727272726,  // delay time (freq) 1/440
		1,      // decay time
		0.2,    // mull.
	)}.play
)

// LeakDC

(
{
    var a;
    a = LFPulse.ar(440, 0.5, 0.5, 0.5);
    [a, LeakDC.ar(a, 0.995)]
}.scope(bufsize: 22050)
)

(
SynthDef(\ks_string, { arg note, pan, rand, delayTime;
	var x, y, env;
	env = Env.new(#[1, 1, 0],#[2, 0.001]);
	// A simple exciter x, with some randomness.
	x = Decay.ar(Impulse.ar(0, 0, rand), 0.1 + rand, WhiteNoise.ar);
 	x = CombL.ar(x, 0.05, note.reciprocal, delayTime, EnvGen.ar(env, doneAction:2));
	x = Pan2.ar(x, pan);
	Out.ar(0, LeakDC.ar(x));
}).add;
)


Synth(\ks_string, [\note, 48.midicps,  \rand, 0.2, \delayTime, 2]);


(
{ // and play the synthdef
	20.do({
		Synth(\ks_string,
			[\note, [48, 50, 53, 58].midicps.choose,
			\pan, 1.0.rand2,
			\rand, 0.1+0.1.rand,
			\delayTime, 2+1.0.rand]);
		[0.125, 0.25, 0.5].choose.wait;
	});
}.fork;
)


(
// white noise
{
	var burstEnv, burst;
	burstEnv = EnvGen.kr(Env.perc(0, 0.01), gate: Impulse.kr(1.5));
	burst = WhiteNoise.ar(burstEnv);
	CombL.ar(burst, 0.2, 0.003, 1.9, add: burst);
}.play;
)

(
// pink noise
{
	var burstEnv, burst;
	burstEnv = EnvGen.kr(Env.perc(0, 0.01), gate: Impulse.kr(1.5));
	burst = PinkNoise.ar(burstEnv);
	CombL.ar(burst, 0.2, 0.003, 1.9, add: burst);
}.play;
)

(
// here we use RLPF (resonant low pass filter) to filter the white noise burst
{
	var burstEnv, burst;
	burstEnv = EnvGen.kr(Env.perc(0, 0.01), gate: Impulse.kr(1.5));
	burst = RLPF.ar(WhiteNoise.ar(burstEnv), MouseX.kr(100, 12000), MouseY.kr(0.001, 0.999));
	CombL.ar(burst, 0.2, 0.003, 1.9, add: burst);
}.play;
)


(
{Pluck.ar(WhiteNoise.ar(0.1), Impulse.kr(2), MouseY.kr(220, 880).reciprocal, MouseY.kr(220, 880).reciprocal, 10, coef:MouseX.kr(-0.1, 0.5)) !2 }.play;
)

(
{SOS.ar(Impulse.ar(2), 0.0, 0.05, 0.0, MouseY.kr(1.45, 1.998, 1), MouseX.kr(-0.999, -0.9998, 1))!2}.play
)