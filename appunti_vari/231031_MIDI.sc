
// step I

Env.adsr().test(0.45);

(
SynthDef.new(\saw,{
    var sig, env;
    sig = Saw.ar(\freq.kr(400) * { Rand(-0.2, 0.2).midiratio}.dup(4));
    sig = Splay.ar(sig);   // porta sig sui 2 canali stereo
    env = Env.adsr().kr(doneAction: 2, gate: \gate.kr(1));
    sig = sig * env * \amp.kr(0.2);
    Out.ar(\out.kr(0), sig);
}).add;
)

x = Synth(\saw, [freq: 440]);
x.set(\gate, 0);

// raffinando


(
{ var ctl = RLPF.ar(Saw.ar(5,0.1), MouseX.kr(2, 200, 1), MouseY.kr(0.01, 1, 1));
    SinOsc.ar(ctl * 200 + 400) * 0.1;
}.play;
)

(
SynthDef.new(\saw,{
    var sig, env;
    sig = Saw.ar(\freq.kr(400) * { Rand(-0.2, 0.2).midiratio}.dup(4));
    sig = Splay.ar(sig);   // porta sig sui 2 canali stereo
    sig = RLPF.ar(sig, \cf.kr(2000), \rq.kr(0.4));
    env = Env.adsr().kr(doneAction: 2, gate: \gate.kr(1));
    sig = sig * env * \amp.kr(0.2);
    Out.ar(\out.kr(0), sig);
}).add;
)

x = Synth(\saw, [freq: 440]);
x = Synth(\saw, [freq: 440, rq: 0.01]);
x.set(\gate, 0);

// software utilizzato VMPK o mia midi

// sistema MIDI: connessione  (anche a server spendto)
MIDIClient.init;
MIDIClient.disposeClient;
// elenca le risorse
MIDIClient.sources;

// input MIDI
MIDIIn.connectAll; // connect to all devices
MIDIIn.disconnectAll;

// oppure andare a mano su jack audio
MIDIFunc.trace(true);  // visualizzazione di debug per i messaggi MIDI
MIDIFunc.trace(false);

// test dei messaggi MIDI con MIDIDef
(
MIDIdef.noteOn(\om, {
    arg vel, note, chan, srcID;
    [note, vel, chan + 1, srcID].postln;
}).permanent_(true);    // sopravvive a CTRL + . (default)
)

MIDIdef(\on).disable;
MIDIdef(\on).enable;
MIDIdef(\on).free; // destry
MIDIdef.freeAll;

(
// ~ar = 128 ! nil;
~ar = Array.fill(128, {nil});

MIDIdef.noteOn(\om, {
    arg val, num;
    ["on", val, num].postln;
    ~ar.put(num, Synth(\saw, [
        freq: num.midicps,
        amp: val.linexp(0, 127, 0.02, 0.2)
    ]));
});

MIDIdef.noteOff(\off, {
    arg val, num;
    ["off", val, num].postln;
    ~ar.at(num).set(\gate, 0);
});
)

MIDIdef.freeAll;

(
MIDIdef.cc(\c70, {
    arg val, num, chan, src;
    [val, num, chan, src].postln;
}, ccNum: 70);     // trovato attivando il debug
)

// esempio usando un control bus

s.meter
s.plotTree;
s.meter;
s.scope; // show bus

~cBus = Bus.control(s, 1);
~cBus.value_(0.7);
~cBus.getSynchronous;   // get current value of a control bus

(
SynthDef.new(\saw,{
    var sig, env, cf;
    cf = In.kr(\cBus.kr(0), 1); // 1 canale
    sig = Saw.ar(\freq.kr(400) * { Rand(-0.2, 0.2).midiratio}.dup(4));
    sig = Splay.ar(sig);   // porta sig sui 2 canali stereo
    sig = RLPF.ar(sig, cf, \rq.kr(0.4));
    env = Env.adsr().kr(doneAction: 2, gate: \gate.kr(1));
    sig = sig * env * \amp.kr(0.2);
    Out.ar(\out.kr(0), sig);
}).add;
)

~ar = Array.fill(128, {nil});

(
MIDIdef.cc(\cf, {
    arg val, num, chan, src;
    [val, num, chan, src].postln;
    ~cBus.set(val.linexp(0, 127, 50, 5000));
}, ccNum: 70);

MIDIdef.noteOn(\om, {
    arg val, num;
    ["on", val, num].postln;
    ~ar.put(num, Synth(\saw, [
        cBus: ~cBus,
        freq: num.midicps,
        amp: val.linexp(0, 127, 0.02, 0.2)
    ]));
});

MIDIdef.noteOff(\off, {
    arg val, num;
    ["off", val, num].postln;
    ~ar.at(num).set(\gate, 0);
});
)






