(
SynthDef(\sin, { arg out=0;
    Out.ar(out,
        FSinOsc.ar(
            Rand(200.0, 400.0),
            0, Line.kr(0.5, 0, 1, doneAction: Done.freeSelf))
    )
}).add;
)

​
(
Routine({
    8.do({
        Synth.new(\sin); 1.0.wait;
    })
}).play;
)

// starting example ...

{ Pulse.ar(XLine.kr(40, 4000, 6), 0.1, 0.2) }.play;

// ... synthdef

Symbol // per vedere la sintassi degli argomenti

(
SynthDef(\pulse, {
    arg freq = 440, harm = 2, width = 0.4, out = 0;

    var sig, env, cf;

    freq = freq * ({ Rand(-0.1, 0.1).midiratio } ! 4);
    cf = freq * harm;

    sig = Pulse.ar(freq, width).sum * 0.1;
    sig = LPF.ar(sig, cf.clip(20, 20000));

    env = EnvGen.kr(
        Env(
            levels: [0, 1, 0],
            times: [\atk.ir(0.01), \rel.ir(0.5)],
            curve: [-2, -4]
        ),
        doneAction: 2
    );
    sig = Pan2.ar(sig, \pan.kr(0), \amp.kr(0.2));
    sig = sig * env;
    Out.ar(out, sig);
}).add;
)

Synth(\pulse, [freq: 440, amp: 0.8]);
Synth(\pulse, [freq: exprand(200, 2000), amp: 0.8]);

// *** pattern: ottenere Routine in modo semplice

// Pseq

(
var a, b;
a = Pseq([1, 2, 3], 2);    // repeat twice
b = a.asStream;
7.do({ b.next.postln; });
)

(
q = Pseq([3, 7, -2, 10], 4);
p = q.asStream;                 // a routine Stream > Routine
)

p.next;

// Prand

(
q = Prand([3, 7, -2, 10], inf);
p = q.asStream;
)

p.next;

// Pxrand: esclude duplicati successivi

(
q = Pxrand([3, 7, -2, 10], inf);
p = q.asStream;
)

p.next;

// Pwrand: pesato

(
q = Pwrand([3, 7, -2, 10], [0.8, 0.2, 0.3], inf);
p = q.asStream;
)

p.next;

[8, 2, 1].normalizeSum
(
q = Pwrand([3, 7, -2, 10], [8, 2, 1].normalizeSum, inf);
p = q.asStream;
)

p.next;

// Pshuf: mescola e ripete versione mescolata

(
var a, b;
a = Pshuf([1, 2, 3, 4, 5], 3);    // repeat 3 times
b = a.asStream;
16.do({ b.next.postln; });
)

(
q = Pseq([Pshuf((1..5), 1)], inf);
p = q.asStream;
)

p.next;

// Prxprand: logarithmic distribution

(
q = Pexprand(1, 9, inf);
p = q.asStream;
)

p.next;

// Pseries: start, step, times

(
q = Pseries(-12, 4, 8);
p = q.asStream;
)

p.next;

// *** ricostruiamo la routine del file precedente

/* step 1*/
(
q = Pwhite(1, 6, inf);
p = q.asStream;
)

p.next;

/* step 2*/
(
q = Pseries(4, 3, 8);
p = q.asStream;
)

p.next;

/* step 3: passo casuale */
(
q = Pseries(-12,  Pwhite(1, 6, inf), inf);
p = q.asStream;
)

p.next;

/* step 4: variazione sul tema */
(
q = Pseries(-12,  Pwhite(1, 6, inf), 10);
p = q.asStream;
)

p.next;

/* step 5: uso delle [] */

(
q = Pseq([Pwhite(1, 6, 4)], 2);
p = q.asStream;
)

p.next;

(
p = Pseq([Pseries(-12, Pwhite(1, 6), 8)], inf);
q = p.asStream;
)

q.next;
q.reset;
(q.next + 60).midicps;

Synth(\pulse, [freq: (q.next + 60).midicps, amp:0.8]);