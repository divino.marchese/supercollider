s.boot;

// *** scorciatoie ****
// CTRL + D browser laterale help co ntestuale (posizionarsi con mouse)
// .. SHIFT + SPACE per help

// visitare Tour of Ugens

{[SinOsc.ar(440), SinOsc.kr(440)]}.plot

s.quit

// ##### a banda limitata ######

{ SinOsc.ar(800,0,0.1) }.scope(1, zoom: 4);
{ SinOsc.ar(XLine.kr(100,15000,6),0,0.1) }.scope(1, zoom: 4);
{ SinOsc.ar(SinOsc.kr(SinOsc.kr(0.2,0,8,10),0, 400,800),0,0.1) }.scope(1, zoom: 4);

// *** Blip tutte le armoniche hanno la stessa ampiezza

{ Blip.ar }.plot(1);

{ Blip.ar(10)}.scope(1);

{ Blip.ar(freq: 440.0, numharm: 1, mul: 1.0, add: 0.0) }.plot();
{ Blip.ar(freq: 440.0, numharm: 2, mul: 1.0, add: 0.0) }.plot();
{ Blip.ar(freq: 440.0, numharm: 10, mul: 1.0, add: 0.0) }.plot();
{ Blip.ar(freq: 440.0, numharm: 100, mul: 1.0, add: 0.0) }.plot();

{ Blip.ar(XLine.kr(20000,200,6),100,0.2) }.scope(1);
{ Blip.ar(XLine.kr(100,15000,6),100,0.2) }.scope(1); // no aliasing
// modulate number of harmonics
{ Blip.ar(200, Line.kr(1,100,20),0.2) }.scope(1);

// *** Saw

{ Saw.ar }.plot;
{ Saw.ar(440, mul: 0.1) }.scope(1);
{ Saw.ar(XLine.kr(20000,200,6),0.2) }.scope(1);
{ Saw.ar(XLine.kr(100,15000,6),0.2) }.scope(1); // no aliasing

// *** Pulse (onda quadra a banda limitata)

{ Pulse.ar }.plot;
{ Pulse.ar(10)}.scope(1);
{ Pulse.ar(XLine.kr(20000,200,6),0.3,0.2) }.scope(1);
{ Pulse.ar(XLine.kr(100,15000,6),0.3,0.2) }.scope(1); // no aliasing

// modulate pulse width
{ Pulse.ar(200, Line.kr(0.01,0.99,8), 0.2) }.scope(1);

// two band limited square waves thru a resonant low pass filter
{ RLPF.ar(Pulse.ar([100,250],0.5,0.1), XLine.kr(8000,400,5), 0.05) }.scope(1);

// *** klang banco di oscillatori

{ Klang.ar(`[ [800, 1000, 1200],[0.3, 0.3, 0.3],[pi,pi,pi]], 1, 0) * 0.4}.scope(1);

{ Klang.ar(`[ {exprand(400, 2000)}.dup(16), nil, nil ], 1, 0) * 0.04 }.scope(1);

// ### a banda non limitata

// *** LFTri triangolare (bipolare da -q a +1 a differenza della sega)

{ LFTri.ar }.plot;
{ LFTri.ar(40) }.scope(1);
{ LFTri.ar(40) }.plot(0.1);

// *** LFSaw

{ LFSaw.ar }.plot;
{ LFSaw.ar(LFSaw.kr(4, 0, 200, 400), 0, 0.1) }.play

