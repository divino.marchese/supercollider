// altro esempio tratto da https://carlcolglazier.com/notes/midi-instrument-control-supercollider/

// dare uno sguardo anche a https://doc.sccode.org/Guides/MIDI.html

MIDIClient.init;
MIDIClient.disposeClient;
MIDIIn.connectAll; // connect to all devices
MIDIIn.disconnectAll;
MIDIFunc.trace(true);  // visualizzazione di debug per i messaggi MIDI
MIDIFunc.trace(false);

(
SynthDef.new(\sinpk, {
    arg freq = 440, gate = 1;
    var sig;
    sig = SinOsc.ar(freq, 0, 0.3) + PinkNoise.ar(0.02);
    sig = EnvGen.kr(Env.asr, gate, doneAction: 2) * sig;
	Out.ar(0, Pan2.ar(sig));
}).add;
)


// https://gist.github.com/umbrellaprocess/973d2aa16e95bf329ee2

~keys = Array.newClear(128);

(
~noteOnFunc = {
    arg val, num, chan, src;
	var node;
	node = ~keys.at(num);
	if (node.notNil, {
		node.release;
		~keys.put(num, nil);
	});
	node = Synth(\sinpk, [\freq, num.midicps]);
	~keys.put(num, node);
};

MIDIdef.noteOn(\on, ~noteOnFunc);

~noteOffFunc = {
    arg val, num, chan, src;
	var node;
	node = ~keys.at(num);
	if (node.notNil, {
		node.release;
		~keys.put(num, nil);
	});
};

MIDIdef.noteOff(\off, ~noteOffFunc);
)


// ******************** MIDI OUTPUT **************************************************

MIDIClient.init;
IDIClient.disposeClient;
MIDIClient.list;
MIDIClient.destinations;
MIDIClient.externalDestinations;


MIDIEndPoint

// noi sceglieremo MIDIEndPoint("Midi Through", "Midi Through Port-0") "Surge XT"


~midiOutput = MIDIOut.newByName("Midi Through", "Midi Through Port-0");

~midiOutput = MIDIOut.newByName("Typhon", "Typhon MIDI 1");

// esempio tratto dalla documentazione
~midiOutput.noteOn(0, 60, 60);
~midiOutput.noteOff(0, 60, 60);
~midiOutput.allNotesOff(16); // non va

~midiOutput.disconnect();

// cancella tutte le note

(
~allOff = {
    (0..127).do({
        arg note;
        ~midiOutput.noteOff(0, note, 60);
});
}
)


~midiOutput.noteOn(0, rrand(21, 100), 60);
~allOff.value();

// CmdPeriod ALT(left) + .
(
f = {"foo".postln };
g = {"bar".postln };
CmdPeriod.add(f);
CmdPeriod.add(g);
)

CmdPeriod.removeAll();




// stop ALT(left) + . lo sostituisce
CmdPeriod.add(~allOff);
CmdPeriod.removeAll;


// tipi di eventi
Event.eventTypes

// default instrument

SynthDef(\default, {
    arg out=0, freq=440, amp=0.1, pan=0, gate=1;
    var z;
    z = LPF.ar(
        Mix.new(
            VarSaw.ar(
                freq + [0, Rand(-0.4,0.0), Rand(0.0,0.4)], // freq
                0, // iphase
                0.3, // width
                0.3) // mul
        ),
        XLine.kr(Rand(4000,5000), Rand(2500,3200), 1) // cutoff
    ) * Linen.kr(gate, 0.01, 0.7, 0.3, 2); // * envelope
    OffsetOut.ar(out, Pan2.ar(z, pan, amp));
});

(
var event = (\dur: 1, \freq: 600); // define an event
event[\dur].postln; // 1
event.play; // play the event
)

// override default synthdef

SinOsc.ar(Rand(300, 500.0)) ) }).add; // overwrite default
(freq: 600).play;
Event.makeDefaultSynthDef; // reset default
(freq: 600).play;

// MIDI event

/* leggere Event Types guide

~midicmd	A Symbol, for the MIDI command to issue
~midiout	A MIDIOut object
~chan	   The MIDI channel number (0-15)

*/

(
(
type: \midi,
midiout: ~midiOutput,
midicmd: \noteOn,
midinote: 60,
sustain:  2,              // time gate = 0, time to off
amp: 0.2,
).play;
)

(
p = Pbind(
    \type, \midi,
    \midiout, ~midiOutput,
    \midicmd, \noteOn,
    \midinote, Pwhite(40, 90, inf),
    \amp, Pexprand(0.1, 0.5, inf),
    \sustain, 0.08,
    \dur, Prand([0.1, 0.2, 0.4], inf)
);
)

q = p.play;
q.stop;

// tratto da https://doc.sccode.org/Tutorials/A-Practical-Guide/PG_Cookbook04_Sending_MIDI.html

(
p = Pbind(
    \type, \midi,
    \midicmd, \noteOn,
    \midiout, ~midiOutput,
    \chan, 0,
    \degree, Pwhite(-7, 12, inf),
    \dur, Pwrand([0.25, Pn(0.125, 2)], #[0.8, 0.2], inf),
   // \legato, sin(Ptime(inf) * 0.5).linexp(-1, 1, 1/3, 3),
    \amp, Pexprand(0.5, 1.0, inf)
).play(quant: 1);
)

p.stop;


