/*** ripresa pattern nidificati e connessi************/

// Pmono There is just one node for the duration of the entire pattern, and it will sustain through each event. If a monophonic phrase requires staccato notes or re-articulation between some notes, see PmonoArtic

p = Pmono(\default, \dur, 0.2, \freq, Pwhite(1,8) * 100 ).play
p.stop

p = Pmono(\default, \dur, 0.2, \freq, Pwhite(1,8) * 100, \detune, [0,2,5,1]).play
p.stop;




// Pseries (step ricalcolato)

Pseries(1,  Pseries(1, 1, inf), inf).asStream.nextN(10);
Pseries(1,  Pwhite(1, 6, inf), inf).asStream.nextN(10);

// Pseq uso di []

Pseq([1, 2, 3, 4], 4).asStream.nextN(16);
Pseq([Pseries(1, 1, 4)], 4).asStream.nextN(16); // the same as above

Pwhite(1, 5, 4).asStream.nextN(8);
Pseq([Pwhite(1, 5, 4)], 4).asStream.nextN(16);
Pseq([Pseries(1, Pwhite(1, 4), 4)], inf).asStream.nextN(16);

// operazioni matematiche

(Pseq([1, 2, 3, 4], 4)*0.5).asStream.nextN(16);


Pseries(0.3,  Pwhite(0, 3, inf)*0.2, inf).asStream.nextN(12);