// Bad construction

(
SynthDef(\fx, {
    var sig;
    sig = SinOsc.ar(freq: \freq.ir(100!2), mul: \amp.ir(0.2));
    // sig = sig * Env.perc(0.01, 0.1).kr(doneAction: 2);  // stoppa il riverbero
    sig = sig * Env.perc(0.01, 0.1).kr(doneAction: 0);
    sig = FreeVerb2.ar(sig[0], sig[1], mix: 0.3, room: 0.99);
    Out.ar(0, sig);
}).add;
)

// Bad construction again

(
SynthDef(\fx, {
    var sig;
    Line.kr(0, 0, 8, doneAction: 2);  // non fa nulla ma libera il server
    sig = SinOsc.ar(freq: \freq.ir(100!2), mul: \amp.ir(0.2));
    // sig = sig * Env.perc(0.01, 0.1).kr(doneAction: 2);  // stoppa il riverbero
    sig = sig * Env.perc(0.01, 0.1).kr(doneAction: 0);
    sig = FreeVerb2.ar(sig[0], sig[1], 0.3, 0.99);
    Out.ar(0, sig);
}).add;
)


s.plotTree

Synth(\fx, [freq: 880]);

// correct approach using Bus

s.options.numAudioBusChannels;
s.options.numControlBusChannels;
s.meter;
s.scope;

(
SynthDef(\reverb, {
    var sig;
    sig = In.ar(\in.ir(2), 2);
    sig = FreeVerb2.ar(sig[0], sig[1], 0.3, 0.99);
    Out.ar(\out.ir(0), sig);
}).add;
)

Symbol

(
SynthDef(\fx, {
    var sig;
    sig = SinOsc.ar(freq: \freq.ir(100)!2, mul: \amp.ir(0.2));
    sig = sig * Env.perc(0.01, 0.1).kr(doneAction: 2);
    sig = FreeVerb2.ar(sig[0], sig[1], 0.3, 0.99);
    Out.ar(\out.ir(0), sig);
    // Out.ar(\out.kr(0), sig);
}).add;
)

Synth(\reverb);
Synth(\fx, [freq: 880, out: 0]); // no effect
Synth(\fx, [freq: rrand(50, 100).midicps, out: 2]);

s.plotTree

(
SynthDef.new(\pulse, {
    arg freq,
    width = 0.4;

    var sig, env, cf;

    freq = freq * ({ Rand(-0.1, 0.1).midiratio } ! 4);
    cf = freq * \harm.ir(2);

    sig = Pulse.ar(freq, width).sum * 0.1;
    sig = LPF.ar(sig, cf.clip(20, 20000));

    env = EnvGen.kr(
        Env(
            levels: [0, 1, 0],
            times: [\atk.ir(0.01), \rel.ir(0.5)],
            curve: [-2, -4]
        ),
        doneAction: 2
    );
    sig = Pan2.ar(sig, \pan.kr(0), \amp.kr(0.2));
    sig = sig * env;
    Out.ar(\out.kr(0), sig);
}).add;
)

Synth(\pulse, [freq: rrand(50, 100).midicps, out: 2]);

// meglio in alcuni casi dare nome ai bus

~reverbBus = Bus.audio(s, 2);
~reverbBus.numChannels;
~reverbBus.index;

Synth(\reverb, [in: ~reverbBus, out: 0]);
Synth(\pulse, [freq: rrand(50, 100).midicps, out: ~reverbBus]);

s.plotTree;

// lavorando coi gruppi: vedi Groups e Order of Execution (Help)

~sourceGroup = Group.new();
// ~fxGroup = Group.new(~sourceGroup, \addAfter);
~fxGroup = Group.after(~sourceGroup);

Synth(\reverb, [in: ~reverbBus, out: 0], ~fxGroup);
Synth(\pulse, [freq: rrand(50, 100).midicps, out: ~reverbBus], ~sourceGroup);





