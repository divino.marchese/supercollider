// **** Ndef uno stile per i NodeProxy ***

/*

Ndef(\a) === Ndef(\a)[0]

DOC
- Ndef
- ProxySpace examples
- Nodeproxy roles

RIFERIMENTI ESTERNI
- https://www.youtube.com/watch?v=PJdyTTZERYs

*/


// tratto dalla documentazione

Ndef(\sound).play;
Ndef(\sound).fadeTime = 1;
Ndef(\sound, { SinOsc.ar([600, 635], 0, SinOsc.kr(2).max(0) * 0.2) });
Ndef(\sound, { SinOsc.ar([600, 635] * 3, 0, SinOsc.kr(2 * 3).max(0) * 0.2) });
Ndef(\sound, { SinOsc.ar([600, 635] * 2, 0, SinOsc.kr(2 * 3).max(0) * 0.2) });
Ndef(\sound, Pbind(\dur, 0.17, \freq, Pfunc({ rrand(300, 700) })) );

Ndef(\lfo, { LFNoise1.kr(3, 400, 800) });
Ndef(\sound).map(\freq, Ndef(\lfo));
Ndef(\sound, { arg freq; SinOsc.ar([600, 635] + freq, 0, SinOsc.kr(2 * 3).max(0) * 0.2) });
Ndef(\lfo, { LFNoise1.kr(300, 400, 800) });

Ndef.clear(3); //clear all Ndefs

// xset Ndef(\sound) is Ndef(\sound)[0]

Ndef(\sound).play;
Ndef(\sound).fadeTime = 4;
Ndef(\sound, {|freq = 440| SinOsc.ar(freq!2, 0, SinOsc.kr(2).max(0) * 0.2) });
Ndef(\sound).xset(\freq, rrand(80, 500));
Ndef.clear(3);

// filter effect

Ndef(\sound)[1] = \filter -> {|in| FreeVerb.ar(in, room: 1.0, damp: 0.4) }

// come dire

(
Ndef(\n_reverb, { |in|
    var sig;
    sig = In.ar(in, numChannels: 2);
    FreeVerb.ar(sig, room: 1.0, damp: 0.4)
});
)

(
Ndef(\sound, {SinOsc.ar([600, 635], 0, SinOsc.kr(2).max(0) * 0.2)});
Ndef(\n_reverb, {FreeVerb.ar(Ndef(\sound).ar, room: 1.0, damp: 0.4)}).play;
);

Ndef.clear;

// altro esempio useo di \wetX (peso dell'effetto)

Ndef(\a, { PinkNoise.ar(0.1.dup) });
Ndef(\a).play;
Ndef(\a)[1] = \filter -> { |in| RLPF.ar(in, LFNoise2.kr(1).exprange(300, 1000), 0.1) };
Ndef(\a).set(\wet1, 0.2); // output = 0.8*in + 0.2*filter(in)
Ndef(\a).clear(3)

// Mix and effect

Ndef(\sound, { SinOsc.ar([600, 635], 0, SinOsc.kr(2).max(0) * 0.2) });
Ndef(\tick, { Decay.ar(Impulse.ar(XLine.kr(1,20,20), 0.25), 0.2, PinkNoise.ar, 0) });

Ndef(\effect).fadeTime = 20;
Ndef(\effect, { Mix.ar([Ndef(\sound).ar, Ndef(\tick).ar])});
Ndef(\effect)[1] = \filter -> {|in| FreeVerb.ar(in, room: 1.0, damp: 0.4) };
Ndef(\effect).play;
Ndef.clear(3)


// audio routing

(
Ndef(\sound, {
    RHPF.ar(
        \in1.ar([0, 0]) * \in2.ar([0, 0]),
        \freq.kr(6000, 2),
        \rq.kr(0.2)
    ) * 7
}).play;
Ndef(\sound).fadeTime = 0.2;    // avoid harsh clicks
)

Ndef(\a, { SinOsc.ar(MouseX.kr(300, 1000, 1) * [1, 1.2], \phase.ar([0, 0]) * 0.2) });
Ndef(\b, { LFDNoise3.ar(MouseY.kr(3, 1000, 1) * [1, 1.2]) });
Ndef(\c, { LFTri.ar(MouseY.kr(3, 10, 1) * [1, 1.2]).max(0) });
Ndef(\a).fadeTime = 0.2;    // avoid harsh clicks again

Ndef(\sound) <<>.in1 Ndef(\a);
Ndef(\sound) <<>.in2 Ndef(\b);
Ndef(\sound) <<>.in2 Ndef(\c);
Ndef(\a) <<>.phase Ndef(\sound);
Ndef(\a) <<>.phase nil;    // unmap
Ndef.clear(3);        // fade out and clear all Ndefs


// *** Tdef ******************


Tdef(\x, { loop { 0.5.wait; "aaaaaaaaaaaaaazz".scramble.postln } }).play;
Tdef(\x, { loop { 0.125.wait; "aazz".scramble.postln } });
Tdef(\x, { loop { 0.5.wait; (note: 14.rand).play } });
Tdef(\x, { loop { 0.5.wait; (note: 14.rand + [0, 3, 6, 7].keep(4.rand)).play } });
Tdef(\x).stop;
Tdef(\x).play;
Tdef.clear;








