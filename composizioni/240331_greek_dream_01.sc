(
SynthDef(\blip, { |freq = 440, sustain = 1.0|
    var osc;
    osc = SinOsc.ar( [freq, freq+0.05.rand], 0.5pi )
    * EnvGen.ar(
        Env.perc, doneAction: Done.freeSelf, levelScale: 0.3, timeScale: sustain
    );
    Out.ar(\out.ir(0), osc);
}).add;
)

(
SynthDef(\wind, { |amp = 0.3|
    var sig;
    sig = SinOsc.ar(
        freq: BrownNoise.ar(100, 200)!2,
        mul: DelayC.kr(LFNoise1.kr(freq: 0.3), 0.2, 0.2)
    );
    sig = sig * amp;
    Out.ar(\out.ir(0), sig);
}).add;
)

(
SynthDef(\reverb, {
    var sig, in;
    in = In.ar(\in.ir(1), numChannels: 2);
    sig = FreeVerb.ar(in, room: 1.0, damp: 0.4);
    Out.ar(\out.ir(0), sig);
}).add;
)

// def bus

~b_fx = Bus.audio(s, 2);

Synth(\reverb, [\in: ~b_fx, \out: 0]);


Synth(\wind, [\amp, 0.1, \out, ~b_fx]);

~dorico = #[64, 65, 69, 71, 72, 76];
~dorico_b = ~dorico -12;

(
var p1, p2, d1, d2, n;
p1 = Prand(~dorico, inf ).asStream.midicps;
p2 = Prand(~dorico_b, inf ).asStream.midicps;
Task({
    12.do({
        arg i;
        d2 = rrand(3,8);
        n = 2*d2;
        ( \instrument: \blip, \freq: p2.next, \sustain: d2, \out: ~b_fx).play;
        n.do({
            d1 = rrand(3,n)*0.06;
            ( \instrument: \blip, \freq: p1.next, \sustain: d1, \out: ~b_fx).play;
            d1.wait;
        });
        d1.wait;
        i.postln;
    });
}).play
)
