# introduzione

Supercollider è un linguaggio ad oggeti. Tocchiamo qui di seguto alcuni argomenti basic.

## oggetti e messaggi

Partiamo col classico *Hello World*
```
"hello world".println;
```
## variabili

Tutti i nomi composti da lettera singola
```
a,b,c,d, ...
```
sono **global** , il nome `s` è risrvato al server. In alternativa variabili globali si dichiarano come
```
~b1, ~freq2, ...
```
le variabili locali sono del tipo 8All'interno di una *function*, di una definizione di *synth* ecc.=
```
var b
```
tutte le variabili vanno inizializzate! I parametri per le *function* si indicano come
```
arg a, b;
```
ovvero
``` 
|a, b|
```
forma che preferiremo.


## function 

Le function si riconoscono per `{...}', ecco un esempio
```
f = {|a, b| a + b};
f;
f.value(2,3);
```
 `f.value(2,3)` ritorna il valora della *function* passati gli argomenti. Consideriamo
```
(
f = { [SinOsc.ar(440, 0, 0.2), SinOsc.ar(442, 0, 0.2)] };
f.value;
)
```
come vediamo in questo caso due *synth* che possiamo far suonare
```
(
f = { [SinOsc.ar(440, 0, 0.2), SinOsc.ar(442, 0, 0.2)] };
f.play;
)
```
per fermarlo lui solo `f.free` (fermeremo tutto con **CTRL+.**)

abbiamo inoltre 
```
f.plot();
```
amche `f.plot` e per sentire in tempo reale e vedere la forma d'onda
```
f.scope
```

## synth 

Possiamo dare un nome ai *synth* usando `SybthDef`
```
// una funzione
{ SinOsc.ar(440, 0, 0.2) }.play;

// un synthdef
SynthDef.new(\tutorial-SinOsc, { Out.ar(0, SinOsc.ar(440, 0, 0.2)) }).play;
```
`\tutorial-SinOsc` è un *symbol*! Provare
```
"ciao" == "ciao";
"ciao" === "ciao";
\ciao === \ciao;
```
possiamo anche scrivere
```
(
SynthDef.new(\tutorial-SinOsc-stereo, { var outArray;
    outArray = [SinOsc.ar(440, 0, 0.2), SinOsc.ar(442, 0, 0.2)];
    Out.ar(0, outArray)
}).play;
)
```
`Out` ci permette di accedere ai *bus*,  ha due *class method* `ar` e `kr`, per i segnali audio e non di controllo usiamo `ar`, il canale stereo `0` è di default l'audio del nostro pc; a dirla bene qui scrivendo 0 ed mandando un segnale stereo usiamo in realtà il *bus* 0 ed 1.
```
// execute first, by itself
SynthDef.new(\tutorial-PinkNoise, { Out.ar(0, PinkNoise.ar(0.3)) }).add;

// then:
x = Synth.new(\tutorial-PinkNoise);
y = Synth.new(\tutorial-PinkNoise);
x.free; 
y.free;    
```
qui stiamo usando un rumore rosa, per visualizzarne lo psetro
```
FreqScope.new(400, 200, 0, server: s);
```
Possiamo inoltre
```
SynthDef(\tutorial-Rand, { Out.ar(0, SinOsc.ar(Rand(440, 660), 0, 0.2)) }).add;
x = Synth(\tutorial-Rand);
y = Synth(\tutorial-Rand);
z = Synth(\tutorial-Rand);
x.free; y.free; z.free;
```

## bus

Partiamo dalle due righe
```
s.options.numInputBusChannels.println;
s.options.numOutputBusChannels.println;
```
`s` è nome di default lasciato al server; se non abbiamo cambiato le impostazioni un 2 sia per la prima che per la seconda istruzione,
```
Out.ar(0, sig)
```
butta, a partire dal *bus* 0 il segnale (a partire in quanto non è detto sia mono).
```
ar (bus: 10, numChannels: 2)
```
legge un segnale stereofonico a partire dal*bus* 10. Fra i bus audio, in un server stereo (default), i primi 4 bus sono riservati e non utilizzabili!
```
(
SynthDef(\tutorial-args, { |freq = 440, out = 0|
    Out.ar(out, SinOsc.ar(freq, 0, 0.2));
}).add;
)

x = Synth("tutorial-args", ["out", 1, "freq", 660]);
y = Synth("tutorial-args", ["out", 1, "freq", 770]);
```
è equivalente ad usare `MIx`, ecco un esempio d'uso
```
{ Mix.new([ PinkNoise.ar(0.1), FSinOsc.ar(801, 0.1), LFSaw.ar(40, 0.1)]) }.play
```
Vediamo come creare ed usare i *bus*
```
b = Bus.audio(s, 2);    
b.index;       
b.numChannels         
c = Bus.control(s);
c.numChannels;        
c.index;        
```
eccp cpsa otteniamo come output
```
-> Bus(audio, 4, 2, localhost)
-> 4
-> 2
-> Bus(control, 0, 1, localhost)
-> 1
-> 0
```
Usiamo quindi i *bus* 
```
(
SynthDef.new(\infreq, { |bus, freqOffset|
    Out.ar(0, SinOsc.ar(In.kr(bus) + freqOffset, 0, 0.5));
}).add;

SynthDef.new(\outfreq, { |freq = 400, bus|
    Out.kr(bus, SinOsc.kr(1, 0, freq/40, freq));
}).add;

b = Bus.control(s,1);
)

(
x = Synth.new(\outfreq, [\bus, b]);
y = Synth.after(x, \infreq, [\bus, b]);
z = Synth.after(x, \infreq, [\bus, b, \freqOffset, 200]);
)
x.free; y.free; z.free; b.free;
```
è utilissimo vedere quali sono i `Node` del server 
```
s.plotTree;
```
Possiamo gesture i *bus* in modo **asincrono**; col metodo `set(... valuues)` passiamo una lista di valori, uno per canale, al bus
```
(
~c1 = Bus.control(s, 1); ~c1.set(880);
~c2 = Bus.control(s, 1); ~c2.set(884);

SynthDef(\map, { |freq1 = 440, freq2 = 440|
   Out.ar(0, SinOsc.ar([freq1, freq2], 0, 0.1));
}).add();
)

x = Synth(\map);
x.map(\freq1, ~c1, \freq2,~c2);
```
possiamo quindi
```
~c2.set(7000);
```



Quindi passiamo ad un esempio articolato studiandolo bene

### compossizione 000

Sempre tratto dal solito tutorial ...
```
(
SynthDef.new(\decayPink, { |outBus = 0, effectBus, direct = 0.5|
    var source;
    source = Decay2.ar(Impulse.ar(1, 0.25), 0.01, 0.2, PinkNoise.ar);
    Out.ar(outBus, source * direct);
    Out.ar(effectBus, source * (1 - direct));
}).add;

SynthDef.new(\decaySin, { |outBus = 0, effectBus, direct = 0.5|
    var source;
    source = Decay2.ar(Impulse.ar(0.3, 0.25), 0.3, 1, SinOsc.ar(SinOsc.kr(0.2, 0, 110, 440)));
    Out.ar(outBus, source * direct);
    Out.ar(effectBus, source * (1 - direct));
}).add;

SynthDef.new(\reverb, { |outBus = 0, inBus|
    var input;
    input = In.ar(inBus, 1);
    16.do({ input = AllpassL.ar(input, 0.04, { Rand(0.001,0.04) }.dup, 3)});
    Out.ar(outBus, input);
}).add;

b = Bus.audio(s,1); 
)

(
x = Synth.new(\reverb, [\inBus, b]);
y = Synth.before(x, \decayPink, [\effectBus, b]);
z = Synth.before(x, \decaySin, [\effectBus, b, \outBus, 1]);
)
```
partiamo con il **riverbero**
```
SynthDef(\reverb, { |outBus = 0, inBus|
    var input;
    input = In.ar(inBus, 1);
    16.do({ input = AllpassL.ar(input, 0.04, { Rand(0.001,0.04) }.dup, 3)});
    Out.ar(outBus, input);
}).add;
```
esso parte da un segnale mono e lo sdoppia 
```
In.ar(inBus, 1);
```
quindi
```
AllpassL.ar(
    input,
    0.04,  // max delay time in s
    { Rand(0.001,0.04) }.dup, // delay 
    3 // amplifica l'output
)
```
se diamo
```
1.dup;
```
otteniamo
```
-> [1, 1]
```
ed il tutto per 16 volte. Vediamo ora la **percussione** e la costruiamo passo per passo: il *timbro*; partiamo dall'*inviluppo**
```

{ Decay2.ar(Impulse.ar(1), 0.015)}.plot();
{ Decay2.ar(Impulse.ar(1), 0.015) - Decay.ar(Impulse.ar(1), 0.01) }.plot();
{ Decay2.ar(Impulse.ar(1), 0.01, 0.015)}.plot();
{ Decay2.ar(Impulse.ar(1, 0.5), 0.015)}.plot(1);
```
le ultime due righe hanno lo stesso effetto; resta da aggiungere il *rumore rosa* e sfasare l'impulso (non necessario)
```
{Decay2.ar(Impulse.ar(1, 0.25), 0.01, 0.2, PinkNoise.ar)}.play;
```
per il *synth* abbiamo
```
SynthDef.new(\decayPink, { |outBus = 0, effectBus, direct = 0.5|
    var source;
    source = Decay2.ar(Impulse.ar(1, 0.25), 0.01, 0.2, PinkNoise.ar);
    Out.ar(outBus, source * direct);
    Out.ar(effectBus, source * (1 - direct));
}).add;
```
dello **strumento** abbiam solo una modulazione in frequenza centrata sui 440HZ che sale e scende di 100Hz con periodo di 3s.

## group

Usando `s.plotTree` abbiamo di fronte i `Node` attivi sul serber, una classe astratta che rappresenta sia i `Synth` che i `Group`. Un esempio
```
(
g = Group.new;
h = Group.before(g);
)

s.plotTree;
g.free; h.free;
```
Possiamo anche
```
g = Group.new;

// make 4 synths in g
// 1.0.rand2 returns a random number from -1 to 1.
4.do({ { arg amp = 0.1; Pan2.ar(SinOsc.ar(440 + 110.rand, 0, amp), 1.0.rand2) }.play(g); });

g.set(\amp, 0.001); // turn them all down

g.free;

s.plotTree;
```

### composizione 001

Ecco un esempio di come gestire suoni ed effetti in modo ordinato!
```
(
SynthDef(\decaySin, { |outBus = 0, effectBus, direct = 0.5, freq = 440|
    var source = Pan2.ar(Decay2.ar(Impulse.ar(Rand(0.3, 1), 0, 0.125), 0.3, 1,
        SinOsc.ar(SinOsc.kr(0.2, 0, 110, freq))), Rand(-1.0, 1.0));
    Out.ar(outBus, source * direct);
    Out.ar(effectBus, source * (1 - direct));
}).add;

SynthDef(\reverb, { |outBus = 0, inBus|
    var input = In.ar(inBus, 2);
    16.do({ input = AllpassC.ar(input, 0.04, Rand(0.001,0.04), 3)});
    Out.ar(outBus, input);
}).add;
)


(
~sources = Group.new;
~effects = Group.after(~sources);     
~bus = Bus.audio(s, 2);         
)

(
x = Synth(\reverb, [\inBus, ~bus], ~effects);
y = Synth(\decaySin, [\effectBus, ~bus, \outBus, 0], ~sources);
z = Synth(\decaySin, [\effectBus, ~bus, \outBus, 0, \freq, 660], ~sources);
)

// we could add other source and effects synths here

~sources.free; ~effects.free; // this frees their contents (x, y, z) as well
~bus.free;

// remove references to ~sources and ~effects environment variables:
currentEnvironment.clear;
```