# modulazione di frequenza ed ampiezza

Lo psettro di un esgnale modulato si espande a seconda del *frequency index modulation*
```
{SinOsc.ar(400 + SinOsc.ar(124, mul: 100), mul: 0.5)}.freqscope(1)
```
SC mette a disposizione pure per la *phase modulation*
```
{PMOsc.ar(400, 124, 1, mul: 0.5)}.scope(1)
```
il terzo argiomento esprime l'indice di modulazione di fase in radianti (paragonabile all'esempio di cui sopra). I tre esempi che seguono sono utili per comprendere l'effetto di una *phase modulation* a livello di altezza e timbro
```
(
{PMOsc.ar(LFNoise0.kr(5, 300, 700),
134, 4)
}.scope(1)
)
(
{PMOsc.ar(700,
LFNoise0.kr(5, 500, 700),
12
)}.scope(1)
)
(
{PMOsc.ar(700, 567,
LFNoise0.kr(5, 6, 12)
)}.scope(1)
)
```
sovrapponendo gli effetti
```
(
{PMOsc.ar(LFNoise0.kr([9, 9], 300, 700),
LFNoise0.kr([9, 9], 500, 700),
LFNoise0.kr([9, 9], 6, 12) //index
)}.scope(1)
)
```
A livello timbrico l'ampiezza dell'indice coll'esempio che segue risulta molto chiaro
```
(
{
	var freq, ratio;
	freq = LFNoise0.kr(4, 20, 60).round(1).midicps;
	ratio = 2/1;
	PMOsc.ar(
		freq, //carrier
		freq*ratio, //modulator
		MouseY.kr(0.1, 10) //index
)}.play
)
```
qui di seguto giocare un po' coi parametri
```
(
{
	var freq, ratio, env, rate = 5, trig;
	trig = Impulse.kr(5);
	freq = TRand.kr([36, 60], [72, 86], trig).midicps;
	ratio = 2;
	env = EnvGen.kr(Env.perc(0, 1/rate), gate: trig);
	PMOsc.ar(
		freq,
		freq*ratio,
		3 + env*4,
		mul: env
)}.play
)
```