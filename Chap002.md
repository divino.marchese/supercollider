# inviluppi

Per arrivare a produrre *symth* fra le prime cose che possiamo fare è lavorare con gli **inviluppi** (di ampiezza e frequenza). Come al solito tavoriamo ad esempi. Data la forma dell'inviluppo (curve con derivata -4)
```
Env.new(levels: [0.0, 0.5, 0.0, 1.0, 0.9, 0.0], times: [0.05, 0.1, 0.01, 1.0, 1.5],curve: -4).test.plot;
```
tale inviluppo vieen attivato da un *trigger*
```
EnvGen.ar(env, gate: Dust.ar(1));
```
e tale inviluppo agisce qui su ampiezza e frequenza
```
(
{
    var env = Env.new([0.0, 0.5, 0.0, 1.0, 0.9, 0.0], [0.05, 0.1, 0.01, 1.0, 1.5], -4);
    var envgen = EnvGen.ar(env, gate: Dust.ar(1));
    SinOsc.ar(
        envgen * 1000 + 440
    ) * envgen * 0.1
}.play
)
```
alcuni esempi di *inviluppi*
```
Env.new.plot;
Env.new(times: [0.5, 1.5]).plot;
Env.new(times: [0.5, 1.5], curve: \sine).plot;
Env.new(times: [0.5, 1.5], curve: 'sine').plot;
Env.new(levels: [0.5, 1.5, 0]).plot;
Env.new(levels: [0.5, 1.5, 1, 0]).plot;
Env.new(levels: [0, 1, 0.3, 0], times: [0.5, 1, 2], curve: [\sine,-3,-2]).plot
Env.new([0, 1, 0.9, 0], [0.1, 0.5, 1],[-5, 0, -5]).plot;
```
per comprendere meglio l'uso dell'argomento `gate` possiamo provare con un *synth* nonimo
```
s.plotTree;

(
x = { |gate = 0|
	var sig, env;
	env = EnvGen.kr(Env.adsr, gate);
	sig = VarSaw.ar(SinOsc.kr(16).range(500,1000))* env;
	}.play;
)

x.set(\gate, 1);
x.set(\gate, 0);

x.free;
s.quit;
```
usando invece i `t_`-*argument* 
```
(
x = {
	|t_gate = 0| 
	var sig, env;
	env = EnvGen.kr(Env.adsr, t_gate);
	sig = VarSaw.ar(SinOsc.kr(16).range(500,1000))* env;
	}.play;
)

x.set(\t_gate, 1);

x.free;
s.quit;
```
ricordiamo che `Env.adsr(attackTime: 0.01, sustainLevel: 1, releaseTime: 1)`. Completiamo il tutto introducendo `doneAction`:
```
s.plotTree;
(
x = {
	|t_gate = 0| 
	var sig, env;
	env = EnvGen.kr(Env.adsr, t_gate, doneAction: 2);
	sig = VarSaw.ar(SinOsc.kr(16).range(500,1000))* env;
	}.play;
)

x.set(\t_gate, 1);

x.free;
s.quit;
```
una volta finito il suo scopo il *node* nel serber viene rilasciato! Aggiungiamo che `test` ha implicito un `doneaction: 2`. 

Altro inviluppo *percussiovo* `Env.perc`
```
Env.perc(attackTime: 0.3, releaseTime: 2, level: 0.4).plot;

Env.perc(0.3, 2, 0.4, 0).plot;

Env.perc(0.05, 1, 1, -4).test.plot;
Env.perc(0.001, 1, 1, -4).test.plot;    // sharper attack
Env.perc(0.001, 1, 1, -8).test.plot;    // change curvature
Env.perc(1, 0.01, 1, 4).test.plot;    // reverse envelope

```
*gade in e fade out* con `Env.triangle`
```
Env.triangle.plot;3// Hear it:4

{SinOsc.ar([440, 442], mul: Env.triangle.kr(2))}.play;
```


