# Klank

Un banco di oscillatori risonanti a frequenze fisse: [qui](https://depts.washington.edu/dxscdoc/Help/Classes/Klank.html) per la documentazione. Partiamo con i primi esempi
```
{ Klank.ar(`[[800], nil, [1]], Impulse.ar(0.5)) }.play;

{ Klank.ar(`[[800, 1071], nil, [1, 1]], Impulse.ar(0.5)) }.play

{ Klank.ar(`[[800, 1071], nil, [1, 1]], Impulse.ar(0.5, mul: 0.1)) }.
```
un po' più random negli impulsi e con più armoniche
```
{ Klank.ar(`[[800, 1071, 1353, 1723], nil, [1, 1, 1, 1]], Dust.ar(8, 0.1)) }.play;
```
vendiamo a due suono flautati, si noti il segnale di eccitazione molto lieve
```
{ Klank.ar(`[[800], nil, [1]], WhiteNoise.ar(0.007)) }.play;

{ Klank.ar(`[[800], nil, [1]], PinkNoise.ar(0.007)) }.play;
```
volutamente ci siamo limitati ad una sola frequenza risonante. Costruiamo quindi un esempio
**step 1**
```
(
{
Klank.ar(`[
    Array.rand(12, 800.0, 4000.0),        
    nil,                                  
    Array.rand(12, 0.1, 2)               
    ], Decay.ar(Impulse.ar(4), 0.03, 0.01))
}.play;
)
```
`Decay.ar(Impulse.ar(4), 0.03, 0.01)` eccitazione brevissima e a bassa intensità. Mettiamoci un rumore (seguendo l'esempio della documentazione)
```
(
{ Klank.ar(`[
	Array.rand(12, 800.0, 4000.0),         
    nil,                             
    Array.rand(12, 0.1, 2)              
], Decay.ar(Impulse.ar(4), 0.03, ClipNoise.ar(mul: 0.01)))
}.play;
)
```
Provare con gli altri rumori! Diversamente eccitato
```
(
{ Klank.ar(`[
	Array.rand(12, 800.0, 4000.0),         
    nil,                             
    Array.rand(12, 0.1, 2)              
], Decay.ar(Dust.ar(4), 0.03, ClipNoise.ar(mul: 0.01)))
}.play;
)
```

## composizione 1

Col nostro flauto

```
(
SynthDef(\flute001, {
	|out=0, pan=0, freq=800, atk=0.5, sus=0.5, rel=0.5|
	var env = EnvGen.kr(Env.linen(atk, sus, rel, 1, 2), doneAction: Done.freeSelf);
	var sig = Klank.ar(`[[freq], nil, [1]], WhiteNoise.ar(0.007));
	Out.ar(out, Pan2.ar(sig*env, pan));
}).add;
)

Synth(\flute001).set(\freq, 800, \atk, 1);

(
r = Routine{
	var t;
    inf.do{|i|
        Synth(\flute001, [
			\atk, rrand(0.2, 0.5),
			\sus, rrand(0.5, 2),
			\rel, rrand(0.5, 2),
			\freq, ([72,77,79,84].choose).midicps,
        ]);
		t = rrand(1.0,4);
		t.postln;
		t.wait;
    }
};
r.play;
)
```

## composizione 2

Uno degli esempi interessanti in documentazione
```
(
SynthDef("help-KlankOverlapTexture",
{|out = 0, freqs = #[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], rings = #[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], atk = 5, sus = 8, rel = 5, pan = 0|
    var e = EnvGen.kr(Env.linen(atk, sus, rel, 1, 4), doneAction: Done.freeSelf);
    var i = Decay.ar(Impulse.ar(Rand(0.8, 2.2)), 0.03, ClipNoise.ar(0.01));
    var z = Klank.ar(
        `[freqs, nil, rings],     // specs
        i                    // input
    );
    Out.ar(out, Pan2.ar(z*e, pan));
}).add;

r = Routine{
    var sustain = 8, transition = 3, overlap = 4;
    var period = transition * 2 + sustain / overlap;
    0.5.wait;            // wait for the synthdef to be sent to the server
    inf.do{
        Synth("help-KlankOverlapTexture", [
            \atk, transition,
            \sus, sustain,
            \rel, transition,
            \pan, 1.0.rand2,
            \freqs, {200.0.rrand(4000)}.dup(12),
            \rings, {0.1.rrand(2)}.dup(12)
        ]);
        period.wait;
    }
};
r.play;
)

r.stop;    // stop spawning new synths
```


