// ************************* LFPulse *********************************

(
SynthDef(\pedestrian,  { |out=0, bal=0|
	var sig = SinOsc.ar(2500, 0, 0.2) * LFPulse.ar(5);
	Out.ar(out, Pan2.ar(sig, bal));
}).add;
)
Synth(\pedestrian);

{LFPulse.ar(5)}.plot(1);


// ************************* LFNoise0 *********************************

(
SynthDef(\lfnoise0,  { |out=0, bal=0|
	var sig = SinOsc.ar(LFNoise0.ar(5).exprange(440, 1000), 0, 0.2);
	Out.ar(out, Pan2.ar(sig, bal));
}).add;
)
Synth(\lfnoise0);


// ************************* LFNoise1 *********************************

(
SynthDef(\lfnoise1,  { |out=0, bal=0|
	var sig = SinOsc.ar(LFNoise1.ar(5).exprange(440, 1000), 0, 0.2);
	Out.ar(out, Pan2.ar(sig, bal));
}).add;
)
Synth(\lfnoise1);


// ************************* LFNoise2 *********************************

(
SynthDef(\lfnoise2,  { |out=0, bal=0|
	var sig = SinOsc.ar(LFNoise1.ar(5).exprange(440, 1000), 0, 0.2);
	Out.ar(out, Pan2.ar(sig, bal));
}).add;
)
Synth(\lfnoise2);







