// blip study

(
SynthDef.new(\blip, {
	arg out;
	var freq, trig, sig;
	freq = LFNoise0.kr(3).exprange(300, 1200).round(300);
	sig = SinOsc.ar(freq)*0.25;
	trig = Dust.kr(2);
	sig = sig * EnvGen.kr(Env.perc(0.01, 0,2), trig);
	Out.ar(out, sig);
	}
).add;


SynthDef.new(\blip1, { |out|
	var sin, sig, trig;
	sin = [SinOsc.ar(300), SinOsc.ar(600), SinOsc.ar(900), SinOsc.ar(1200)];
	sig = TWChoose.ar(LFNoise0.ar(3), sin, (1!4).normalizeSum) * 0.2;
	trig = Dust.kr(2);
	sig = sig * EnvGen.kr(Env.perc(0.01, 0,2), trig);
	Out.ar(out, sig);
	}
).add;

SynthDef.new(\reverb, {
	arg in, out=0;
	var sig;
	sig = In.ar(in, 1);
	sig = FreeVerb.ar(sig, 0.5, 0.8, 0.2)!2;
	Out.ar(out, sig);
	}
).add;
)

y = Synth.new(\reverb, [\in, 6]);
x = Synth.new(\blip, [\out, 6]);
