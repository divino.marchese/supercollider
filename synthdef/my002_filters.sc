// ************************* Formlet ****************************


{ Impulse.ar(20, 0.5)}.play;
{ Formlet.ar(Impulse.ar(20, 0.5), freq: 440, attacktime: 0.01, decaytime: 0.5) }.play;

{ Formlet.ar(Impulse.ar(440, 0.5,0.1),MouseX.kr(300,3000,'exponential'), 0.01, MouseY.kr(0.1,1.0,'exponential')) }.play

{ Formlet.ar(Dust.ar(10), freq: 440,C0.01, decaytime: 0.5) }.play;

(
{
	var sig, pitch;
	sig = Dust.ar(8);
	pitch = LFNoise0.kr(4).range(449, 300).round(100);
	Formlet.ar(sig,
		freq: pitch,
		attacktime: 0.01,
		decaytime: 0.5);
}.play;
)

(
{
    var in;
    in = Blip.ar(SinOsc.kr(5,0,20,300), 1000, 0.1);
    Formlet.ar(in, XLine.kr(1500,700,8), 0.005, 0.04);
}.play;
)

(
// mouse control of frequency and decay time.
{
    var in;
    in = Blip.ar(SinOsc.kr(5,0,20,300), 1000, 0.1);
    Formlet.ar(in,
        MouseY.kr(700,2000,1),
        0.005, MouseX.kr(0.01,0.2,1));
}.play;
)

(
// mouse control of frequency and decay time.
{
    var freq;
    freq = Formlet.kr(
        Dust.kr(10 ! 2),
        MouseY.kr(7,200,1),
        0.005, MouseX.kr(0.1,2,1)
    );
    SinOsc.ar(freq * 200 + [500, 600] - 100) * 0.2
}.play;
)

s.quit;