(
{
	var sig, strike, env, noise, pitch, delayTime, detune;
	strike = Impulse.ar(0.01);
	env = Decay2.ar(strike, 0.008, 0.04);
	pitch = (36 + 54.rand);
	sig = Mix.ar(Array.fill(3, { |i|
		detune = #[-0.05, 0, 0.04].at(i);
		delayTime = 1 / (pitch + detune).midicps;
		noise = LFNoise2.ar(3000, env);
		CombL.ar(noise,
			delayTime, 		// max delay time
			delayTime,	    // actual delay time
			6)});			// decay time of string
		Pan2.ar(sig, (pitch - 36)/27 - 1)
}.play
)
