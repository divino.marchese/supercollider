// tick

(
SynthDef.new(\tick, {|out=0, freq1=300, freq2=500, freq3=100, t1=0.001, t2=0.02|
	var freqEnv = EnvGen.kr(Env.new(
		levels: [freq1, freq2, freq3], times: [t1, t2],curve: -4));
	var sigEnv = EnvGen.ar(Env.new(
		levels: [0, 1, 0], times: [t1, t2],curve: -4), doneAction: 2);
	var sig = SinOsc.ar(freq: freqEnv, mul: sigEnv);
	Out.ar(out, Pan2.ar(sig, 0));
}).add;
)

Synth(\tick);

Synth(\tick, [\freq1, 100, \freq2, 1000, \freq3, 200, \t2, 0.1]);
Synth(\tick);

