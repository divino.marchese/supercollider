# buffer

Dalla classe `Server`, [qui](https://doc.sccode.org/Classes/Server.html) per le API, e `ServerOptions`, [qui](https://doc.sccode.org/Classes/ServerOptions.html) per le API
```
s.sampleRate;
s.options.numInputBusChannels.println;
```
si evince la frequenza di campionamento per i canali in ingresso, di default 44.1 kHz per un ingresso stereo. Per allocare un *buffer* stereo e liberarlo possiamo proceder nel modo seguente (un *buffer* da  `8s` `2`  canali a '44100 Hz' per canale)
```
b = Buffer.alloc(s, s.sampleRate * 8.0, 2); 
b.free;
```
Ecco come avere le informazioni di un buffer
```
b.bufnum;
b.numFrames;
b.numChannels;
b.sampleRate;
```
Proviamo quindi con uesempio commentandolo nel dettaglio poi
```
b = Buffer.read(s, Platform.resourceDir +/+ "sounds/a11wlk01.wav");

(
x = SynthDef("playBuf1",{ arg out = 0, bufnum;
    Out.ar( out,
        PlayBuf.ar(1, bufnum, BufRateScale.kr(bufnum), doneAction: 2)
    )
}).play(s,[\bufnum, b.bufnum]);
)

x.free; b.free;
```
`PlayBuf`, [qui](https://doc.sccode.org/Classes/PlayBuf.html) per le API lo troviamo
```
PlayBuf.ar(numChannels, bufnum: 0, rate: 1.0, trigger: 1.0, startPos: 0.0, loop: 0.0, doneAction: 0)
```
`doneAction: 2` risulterà essenziale nei *loop*; ad ogni *buffer* viene associato, fra le varie info, un numero ed il numero di canali, per `rate` abbiamo
- `1.0` il *samplerate* del server 
- `2.0` un ottava sopra  
- `0.5` un ottava sotto  
- `1.0` al contrario
- `BufRateScale.kr(bufnum)` scala rispetto al server (se il *buffer* è a `44.1KHz` la scala è `1` ).
```
(
x = SynthDef("playBuf2",{ arg out = 0, bufnum, startPos;
    Out.ar( out,
        PlayBuf.ar(1, bufnum, BufRateScale.kr(bufnum), 
			startPos:  BufFrames.kr(bufnum)*startPos, doneAction: 2)
    )
}).play(s,[\bufnum, b.bufnum, \startPos, 0.5]);
)
```
usiamo:
- `BufFrames.kr(bufnum)` permette di recuperare `b.numFrames`, `startPos` diviene quindi una frazione.
Ed ancora
```
(
x = SynthDef("playBuf3",{ arg out = 0, bufnum, startPos;
    Out.ar( out,
        PlayBuf.ar(1, bufnum, BufRateScale.kr(bufnum), 
			startPos:  BufFrames.kr(bufnum)*startPos, 
			trigger: Dust.kr(1),
			doneAction: 0)
    )
}).play(s,[\bufnum, b.bufnum, \startPos, 0.5]);
)
``
qui il `doneAction: 0` è d'obbligo ... altrimenti, se finisse il campione il trigger non riattiverebbe il successivo.


Senza appesantire tropppo la memoria possiamo lavorare su disco
```
(
SynthDef(\uffer-cue,{ |out=0, bufnum!
    Out.ar(out,
        DiskIn.ar( 1, bufnum )
    )
}).add;
)

b = Buffer.cueSoundFile(s,Platform.resourceDir +/+ "sounds/a11wlk01-44_1.aiff", 0, 1);
y = Synth.new(\uffer-cue, [\bufnum, b], s);

b.free; y.free;
```
Se volessimo caricare i campioni audio nella cartella `audio` di un nostro progetto potremmo impostare
```
~audio = PathName.new(thisProcess.nowExecutingPath).parentPath ++ "/audio";
```

## sintesi granulare

La **sintesi granulare** ci permette di introdurre ulteriori dettagli. Prima di tutti vediamo il motore della soluzione; una *routine*, ne parleremo nei capitoli successivi
```
{ 4.do({ "Threadin...".postln; 1.wait;}) }.fork;
```
`fork` reasforma una *function* in una *rourine*, essa ci permette di suonare - `play`, ogni singola *function*. Un **grano** è un frammento di segnale audio 
```
(
SynthDef(\grain, { |bufnum=0, pan=0.0, startPos=0.0, amp=0.1, dur=0.04|  
	var grain = PlayBuf.ar(1, bufnum, BufRateScale.kr(bufnum),
		startPos: BufFrames.kr(bufnum)*startPos, 
		)*(EnvGen.kr(Env.perc(0.01,dur),
		doneAction: 2) -0.001);
	Out.ar(0, Pan2.ar(grain, pan))}).add; 
)

Synth(\grain,[\bufnum, b.bufnum, \startPos, rrand(0.0,1.0), \amp, exprand(0.005,0.1), \pan, 1.0.rand2]);  
```
quindi operiamo con la sintesi
```
({
200.do{|i|
    var timeprop = (i/199.0)**3;
	Synth(\grain,[\bufnum, b.bufnum, \startPos,rrand(0.0,timeprop),\amp, exprand(0.005,0.1), \pan, 1.0.rand2]);  
	max(rrand((timeprop*0.1), 0.01),timeprop*0.4).wait
}; 
}.fork
)
```
il *buffer* viene percorso per esteso con un attesa
