# eventi

Come per il caso della sintesi granulare talvolta gestire eventi mediante *clock* può essere 
```
SystemClock.sched(5, { "hello".postln });
SystemClock.sched(5, { "hello".postln; 1});
```
Nel secondo la schedulazione i ripresenta dopo `1s`
```
(
SystemClock.sched(0.0,{ arg time;
    time.postln;
    rrand(0.1,0.9)
});
)
```
`arg` è il tempo che passa il *clock*.

## routine e task

Come da documentazione, una *routine* permette di eseguire ed interrompere una *function*, essa è un *thread*; mediante il *message* `next` si passaa al successiovo `yeld` (un `return non che non fa uscire). `Routine` estende `Stream`
``` 
(
r = Routine({
    "alpha".yield;
    "beta".yield;
    "gamma".yield;
    "delta".yield;
    "epsilon".yield;
    "phi".yield;
});
)

r.next;   // r.value 
6.do({ r.next.postln });
```
possiamo anche schedulare una *routine* usando `play`
```
(
r = Routine({ loop{
	"  *  ".postln; 0.5.wait; 
	" *** ".postln; 0.5.wait;
	"*****".postln; 0.5.wait; 
	} 
});
)

r.play;
```
Con gli argomenti
```
(
r = Routine { arg inval;
    inval.postln;
    inval = 123.yield;
    inval.postln;
}
)

r.next("hello routine");
r.next("goodbye routine");
```
schedulare una *routine* come evento
```
(
r = Routine({
    var delta;
    loop {
        delta = rrand(1, 3) * 0.5;
        "Will wait ".post; delta.postln;
        delta.yield;
    }
});
)

TempoClock.default.sched(0, r);

r.stop;
```
Quindi un esempio suonato
```
(
SynthDef(\singrain, { |freq = 440, amp = 0.2, sustain = 1|
    var sig;
    sig = SinOsc.ar(freq, 0, amp) * EnvGen.kr(Env.perc(0.01, sustain), doneAction: 2);
    Out.ar(0, sig!2);  
}).add;

r = Routine({
    var delta;
    loop {
        delta = rrand(1, 3) * 0.5;
        Synth(\singrain, [freq: exprand(200, 800), amp: rrand(0.1, 0.5), sustain: delta * 0.8]);
        delta.yield;
    }
});
)

r.next;
```
Le *routine* per quello che sono non offrono grossi spunti musicali, serve quindi un passo avanti con i `Task`: un `Task` è una `Routine` che può essere messa in pausa e viene costruito attorno ad essa!
```
(
t = Task({
    loop {
        [60, 62, 64, 65, 67, 69, 71, 72].do({ |midi|
            Synth(\singrain, [freq: midi.midicps, amp: 0.2, sustain: 0.1]);
            0.125.wait;
        });
    }
}).play;
)

// probably stops in the middle of the scale
t.stop;

t.play;    // should pick up with the next note

t.stop;
```
in forma anonima come *routine*
```
(
{
loop {
   [60, 62, 64, 65, 67, 69, 71, 72].do({ |midi|
       Synth(\singrain, [freq: midi.midicps, amp: 0.2, sustain: 0.1]);
       0.125.wait;
   });
}
}.fork;
)
```
4 volte più veloce
```
(
var tempoclock = TempoClock(4);
{
loop {
   [60, 62, 64, 65, 67, 69, 71, 72].do({ |midi|
       Synth(\singrain, [freq: midi.midicps, amp: 0.2, sustain: 0.1]);
       0.125.wait;
   });
}
}.fork(tempoclock);
)
```
usando *routine* dentro task
```
(
var midi, dur;
midi = Routine({
    [60, 72, 71, 67, 69, 71, 72, 60, 69, 67].do({ |midi| midi.yield });
});
dur = Routine({
    [2, 2, 1, 0.5, 0.5, 1, 1, 2, 2, 3].do({ |dur| dur.yield });
});

SynthDef(\smooth, { |freq = 440, sustain = 1, amp = 0.5|
    var sig;
    sig = SinOsc.ar(freq, 0, amp) * EnvGen.kr(Env.linen(0.05, sustain, 0.1), doneAction: 2);
    Out.ar(0, sig ! 2)
}).add;

r = Task({
    var delta;
    while {
        delta = dur.next;
        delta.notNil
    } {
        Synth(\smooth, [freq: midi.next.midicps, sustain: delta]);
        delta.yield;
    }
}).play(quant: TempoClock.default.beats + 1.0);
)
```
è ovvio che una tale soluzione potrà essere scritta in modo molto più semplice!