# sintesi sottrattiva

Abbiamo i soliti filtri più quelli risonanti ....
```
(
{
	var signal, filter, cutoff, resonance;
	signal = PinkNoise.ar(mul: 0.7);
	signal = Saw.ar([30, 35], mul: 0.7);
	cutoff = MouseX.kr(40, 10000, 1);
	resonance = MouseY.kr(0.01, 2.0); // rq
	// RLPF.ar(signal, cutoff, resonance)
	// RHPF.ar(signal, cutoff, resonance)
	BPF.ar(signal, cutoff, resonance)
}.scope(1)
)

{SinOsc.ar(LFNoise0.kr(12, 500, 500), mul: 0.5)}.play

{RLPF.ar(PinkNoise.ar(0.3), LFNoise0.kr([12, 12], 500, 500), 0.02)}.play
```
DA COMPLETARE


## risonatori klank

Strumento potentissimo
```
{ Klank.ar(`[[800, 1071, 1153, 1723], nil, 1!4], Impulse.ar(freq: 2,mul: 0.1)) }.play;
{ Klank.ar(`[[800, 1071, 1353, 1723], nil, 1!4], Dust.ar(8, 0.1)) }.play;
{ Klank.ar(`[[800, 1071, 1353, 1723], nil, 1!4], PinkNoise.ar(0.007)) }.play;
{ Klank.ar(`[[200, 671, 1153, 1723], nil, 1!4], PinkNoise.ar([0.007, 0.007])) }.play;
```
attnezione. usare apice per non dar luogo ad interpretazione multicanale; partiamo con un esempio armonico
```
Array.rand(10,1,5)

(
play({
    Klank.ar(`[
        Array.rand(12, 800.0, 4000.0),         // frequencies
        nil,                                   // amplitudes (default to 1.0)
        Array.rand(12, 0.1, 2)                 // ring times
        ], Decay.ar(Impulse.ar(4), 0.03, ClipNoise.ar(0.01))) // input
})
)
```
un esempio *multichannel*
```
{ Klank.ar(`[[[800, 6000], 1071, [1153, 8000], 1723], nil, [1, 1, 1, 1]], Impulse.ar([2, 3], 0, 0.1)) }.play;
```
ed ecco una definizione di *sinth* con test
```
(
SynthDef(\ohm, { |out=0, freq=100, dur=3|
	var sig, env;
	sig = Klank.ar(
		`[(1..10)*freq, // freq array
			[0.05, 0.2, 0.04, 0.06, 0.11, 0.01, 0.15, 0.03, 0.15, 0.2]], // ampl
		PinkNoise.ar(MouseX.kr(0.01, 0.1))); // input
	env = EnvGen.ar(Env.new(levels: [0, 1, 1, 0], times: [0.5, dur-2, 1.5], curve: [2, 0, 1]), doneAction: 2);
	sig = env*sig;
	Out.ar(out, sig!2)
}).add;
)

Synth(\ohm, [\freq, 80, \dur, 5]);
```
ecco quindi un "biplano"
```
{ClipNoise.ar(0.1)}.freqscope; // enfasi sulle alte freq
{WhiteNoise.ar(0.1)}.freqscope;


Array.series(size: 10, start: 50, step: 50)

(
{
Klank.ar(
		`[Array.series(10, 50, 50),
			Array.series(10, 1.0, -0.1)],
		ClipNoise.ar(0.01))
}.scope(1)
)
```
risonatori con `exprand`
```
(
{
	Klank.ar(
		`[{exprand(100, 10000)}!20],
		PinkNoise.ar(0.005)
	)
}.scope(1);
)
```
un "suono percussiovo"
```
(
{
	var chime, freqSpecs, burst, totalHarm = 10;
	var burstEnv, att = 0, burstLength = 0.0001;
	freqSpecs = `[{rrand(200, 500)}!totalHarm, //freq array
		({rrand(0.3, 1.0)}!totalHarm).normalizeSum.round(0.01), //amp array
		{rrand(2.0, 4.0)}!totalHarm]; //decay rate array
	burstEnv = Env.perc(0, burstLength); // envelope times
	burst = PinkNoise.ar(EnvGen.kr(burstEnv, gate: Impulse.kr(1))); //Noise burst
	Klank.ar(freqSpecs, burst)*MouseX.kr(0.1, 0.8)
}.scope(1)
)
```
uno "più armonico" (usando frequenze commensurabili) tipo "boing boing"
```
(
SynthDef("help-GrayNoise", { arg out=0;
    Out.ar(out,
        GrayNoise.ar(0.1)
    )
}).play;
)

(
{
	var chime, freqSpecs, burst, totalHarm = 10;
	var burstEnv, att = 0, burstLength = 0.0001;
	freqSpecs = `[{Array.series(10,200,50).choose}!totalHarm , //freq array
		({rrand(0.3, 1.0)}!totalHarm).normalizeSum.round(0.01), //amp array
		{rrand(2.0, 4.0)}!totalHarm]; //decay rate array
	burstEnv = Env.perc(0, burstLength); // envelope times
	burst = GrayNoise.ar(EnvGen.kr(burstEnv, gate: Impulse.kr(1))); //Noise burst
	Klank.ar(freqSpecs, burst)*MouseX.kr(0.04, 0.1)
}.scope(1)
)
```
il `GrayNoise` enfatizza le basse frequenze.


## composizione 501

```
(
{
	var totalInst, totalPartials, baseFreq, ampControl, chimes, cavern;
	totalInst = 5; // Total number of chimes
	totalPartials = 12; // Number of partials in each chime
	baseFreq = rrand(200, 1000); // Base frequency for chimes
	// il canto
	chimes = Mix.ar({
		Pan2.ar(
			Klank.ar(`[
				{baseFreq*rrand(1.0, 12.0)}!totalPartials,
				Array.rand(totalPartials, 0.3, 0.9),
				Array.rand(totalPartials, 0.5, 6.0)],
			Decay.ar(
				Dust.ar(0.2, 0.02), //Times per second, amp
				0.001, //decay rate
				PinkNoise.ar // Noise
		)), 1.0.rand2) // Pan position
	}!totalInst
	);
	// il basso percussivo
	cavern = Mix.ar(
		{
			var base;
			base = exprand(50, 500);
			Klank.ar(
				`[
					{rrand(1, 24) * base *
						rrand(1.0, 1.1)}!totalPartials,
					Array.rand(10, 1.0, 5.0).normalizeSum],
				GrayNoise.ar( [rrand(0.03, 0.1), rrand(0.03, 0.1)])
			)*max(0, LFNoise1.kr(3/rrand(5, 20), mul: 0.005))
	}!5);
	// le voci sommate
	cavern + chimes
}.play
)			

s.quit;
```

## composizione 502

Provare a commentare la riga del filtro risonante per ottenere effetti diversi!
```
(
SynthDef.new(\KSpluck3, { |midiPitch, art|
	var burstEnv, att = 0, dec = 0.01, legalPitches; 
	var out, delayTime;
	delayTime = [midiPitch, midiPitch + 12].midicps.reciprocal;
		burstEnv = EnvGen.kr(Env.perc(att, dec));
	out = PinkNoise.ar([burstEnv, burstEnv]); 
	out = CombL.ar(out, delayTime, delayTime,
		art, add: out); 
	out = RLPF.ar(out, LFNoise1.kr(2, 2000, 2100), 0.1); 
	DetectSilence.ar(out, doneAction: 2);
	Out.ar(0, out*0.8)
}
).add;
)
//Then run this routine
(
r = Task({
{Synth("KSpluck3",
[
\midiPitch, [0, 2, 4, 6, 8, 10].choose + [24, 36, 48, 60].choose,
\art, [0.125, 0.25, 0.5, 1.0, 2.0].choose
]);
//Choose a wait time before next event
[0.125, 0.125, 0.125, 0.125, 0.125, 1].choose.wait;
}.loop;
}).play(SystemClock)
)

```



## delay con CombL

Con `CombN`, nessuna interpolazione e `CombC` ad interpolazione cubica `CombL` è un *comb delay* ad interpolazione lineare; 
```
CombL.ar(in: 0, maxdelaytime: 0.2, delaytime: 0.2, decaytime: 1, mul: 1, add: 0)
```
- **in** The input signal.
- **maxdelaytime**	The maximum delay time in seconds. Used to initialize the delay buffer size.
- **delaytime**	Delay time in seconds.
- **decaytime**	Time for the echoes to decay by 60 decibels. If this time is negative then the feedback coefficient will be negative, thus emphasizing only odd harmonics at an octave lower.
- **mul**	Output will be multiplied by this value.
- **add** This value will be added to the output.  

qui sotto a confronto
```
{ CombN.ar(WhiteNoise.ar(0.01), 0.01, XLine.kr(0.0001, 0.01, 20), 0.2) }.play;
{ CombL.ar(WhiteNoise.ar(0.01), 0.01, XLine.kr(0.0001, 0.01, 20), 0.2) }.play;
{ CombC.ar(WhiteNoise.ar(0.01), 0.01, XLine.kr(0.0001, 0.01, 20), 0.2) }.play;

{ CombN.ar(WhiteNoise.ar(0.01), 0.01, XLine.kr(0.0001, 0.01, 20), -0.2) }.play;
{ CombL.ar(WhiteNoise.ar(0.01), 0.01, XLine.kr(0.0001, 0.01, 20), -0.2) }.play;
{ CombC.ar(WhiteNoise.ar(0.01), 0.01, XLine.kr(0.0001, 0.01, 20), -0.2) }.play;
```
ma vediamo subito come lavora
```
{ Impulse.ar(freq: 1, mul: 2) }.play;
{ CombN.ar(Impulse.ar(1), 0.5, XLine.kr(0.01, 0.5, 20), 1) }.play;
{ CombN.ar(Impulse.ar(1), 0.01, XLine.kr(0.0001, 0.01, 20), 0.2) }.play;
```
Con la sintesi sottrattiva si possono emulare le campane:
```
(
{
var chime, freqSpecs, burst, harmonics = 10;
var burstEnv, burstLength = 0.001;
freqSpecs = `[
	{rrand(100, 1200)}.dup(harmonics), //freq array
	{rrand(0.3, 1.0)}.dup(harmonics).normalizeSum, //amp array
	{rrand(2.0, 4.0)}.dup(harmonics)]; //decay rate array
burstEnv = Env.perc(0, burstLength); //envelope times
burst = PinkNoise.ar(EnvGen.kr(burstEnv, gate: Impulse.kr(1))*0.4); //Noise burst
Klank.ar(freqSpecs, burst)!2
}.play
)
```
Confrontiamo i due analoghi: i)
```
(
{
	var burstEnv, att = 0, dec = 0.001;
	var out, delayTime = 0.5, delayDecay = 0.7;
	burstEnv = EnvGen.kr(Env.perc(att, dec), gate: Impulse.kr(1/delayDecay));
	out = PinkNoise.ar(burstEnv);
	out = out + CombL.ar(out, delayTime, delayTime, delayDecay); 
	out
}.play 
)
```
e ii) ... si osservi il `delay`
```
(
{
	var burstEnv, att = 0, dec = 0.001;
	var burst, delayTime, delayDecay = 2;
	var midiPitch = 69; // A 440
	delayTime = midiPitch.midicps.reciprocal;
	burstEnv = EnvGen.kr(Env.perc(att, dec), gate: Impulse.kr(1/delayDecay));
	burst = PinkNoise.ar(burstEnv);
	CombL.ar(burst, delayTime, delayTime,
		delayDecay, add: burst);
}.play
)
```
con oscillatori armonici (decommentare per sentire la differenza)
```
(
{
	var sig, trig;
	trig = Impulse.kr(5);
	// trig = Impulse.kr(Array.fill(5, {rrand(4.0, 7.0)}));
	sig = SinOsc.ar(TRand.kr(2000, 4000, trig), mul: EnvGen.kr(Env.perc(0.001, 0.1),trig))*0.1;
	Mix.ar(Pan2.ar(
		CombL.ar(
			sig, 
			2.0, 
			Array.fill(5, {rrand(0.5, 1.9)})), 
		Array.fill(5, {2.0.rand2})));
}.play
)
```

